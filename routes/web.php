<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cart/set', 'CartController@setCart')->name('cart.set');
Route::get('/cart/calculation', 'Api\CartController@calculationFee')->name('cart.calculation');

Route::group(['middleware' => ['api-lang']], function () {
    Route::get('/roads', 'Api\MapController@roads')->name('roads.get-all');
    Route::get('/notifications', 'Api\NotificationsController@userNotifications')->name('notifications');
    Route::patch('/notifications/mark-read/{id?}', 'Api\NotificationsController@markRead')
        ->name('notifications.mark-read');

    Route::get('/profile/vignettes', 'Api\ProfileController@vignettes')->name('profile.get-vignettes');
    Route::get('/profile/get-signs', 'Api\UserSignController@pagination')->name('profile.get-signs');
    Route::delete('/profile/signs/{id}', 'Api\UserSignController@remove')->name('profile.remove');
    Route::post('/profile/signs', 'Api\UserSignController@store')->name('profile.add-sign');
    Route::post('/profile', 'Api\ProfileController@update')->name('profile.update');

    Route::post('/vignettes/validate', 'ChooseVignetteController@vignettesValidate')->name('vignettes.validate');
    Route::get('/vignettes/check', 'Api\VignetteController@check')->name('vignettes.check');

    Route::post('/cart/add', 'CartController@add')->name('cart.add');
    Route::put('/cart/contacts', 'Api\CartController@updateContacts')->name('order.update');
    Route::get('/cart/data', 'Api\CartController@getCart')->name('cart.items');
    Route::get('/cart/has-items', 'Api\CartController@hasItems')->name('cart.has-items');
    Route::get('/cart/success-order', 'Api\CartController@successOrder')->name('cart.success-order');
    Route::post('/cart/add', 'Api\CartController@add')->name('cart.add');
    Route::delete('/cart/remove/{id}', 'Api\CartController@remove')->name('cart.remove');

    Route::post('/order/store', 'Api\CartController@createOrder')->name('order.store');
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::resource('/road', 'RoadController', ['as' => 'admin'])->except(['show']);

    Route::get('/history', 'UserHistoryController@index')->name('admin.history');
    Route::get('/history/{historyId}', 'UserHistoryController@show')->name('admin.history.show');
    Route::get('/history/details/{id}', 'UserHistoryController@details')->name('admin.history.details');
    Route::post('/history/search', 'UserHistoryController@search')->name('admin.history.search');

    Route::get('/vignette', 'VignetteController@index')->name('admin.vignette.index');
    Route::get('/vignette/create', 'VignetteController@create')->name('admin.vignette.create');
});

Route::group(['prefix' => '{lang?}', 'middleware' => ['lang']], function() {
    Route::get('/cart', 'CartController@show')->name('cart.show');

    Route::get('/vignettes', 'ChooseVignetteController@index')->name('vignettes');

    Route::group(['prefix' => 'profile', 'middleware' => ['auth']], function () {
        Route::get('/', 'ProfileController@show')->name('profile.show');
        Route::get('/my-vignettes', 'ProfileController@vignettes')->name('profile.vignettes');
        Route::get('/signs', 'ProfileController@signs')->name('profile.signs');
        Route::get('/notifications', 'ProfileController@notifications')->name('profile.notifications');
    });
});
