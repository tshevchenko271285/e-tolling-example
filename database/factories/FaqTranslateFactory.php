<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\FaqTranslate>
 */
class FaqTranslateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'lang' => config('app.fallback_locale', 'ua'),
            'question' => fake()->realText(30),
            'answer' => fake()->realText(255),
        ];
    }
}
