<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderItem>
 */
class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'product_id' => null,
            'car_country' => fake()->country,
            'car_sign' => fake()->bothify('?? #### ??'),
            'started_at' => now()->toDateTimeString(),
            'finished_at' => now()->addDays($this->faker->randomElement([10, 30, 365]))->toDateTimeString(),
            'product_data' => null,
        ];
    }
}
