<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => null,
            'manager_id' => null,
            'type_of_person' => null,
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'country' => $this->faker->country,
            'postcode' => $this->faker->postcode,
            'address' => $this->faker->address,
            'city' => $this->faker->city,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email,
            'company_name' => $this->faker->company,
            'tax_number' => $this->faker->randomNumber(),
            'payment_type' => null,
            'paid' => 1,
        ];
    }
}
