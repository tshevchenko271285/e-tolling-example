<?php

namespace Database\Factories;

use App\Models\MenuItem;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\MenuItem>
 */
class MenuItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'menu_id' => null,
            'parent_id' => null,
            'name' => $this->faker->word,
            'type' => MenuItem::TYPE_LINK,
            'content' => $this->faker->word,
            'sorting' => 1,
            'visible' => MenuItem::VISIBLE_YES,
        ];
    }
}
