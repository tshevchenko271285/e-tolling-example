<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word,
            'price' => $this->faker->randomFloat(2, 0, 99),
            'term' => $this->faker->randomElement([10, 30, 365]),
            'change_status' => $this->faker->randomElement(Product::AVAILABLE_CHANGE_STATUS),
        ];
    }
}
