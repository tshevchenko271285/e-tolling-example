<?php

namespace Database\Seeders;

use App\Models\Reminder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;

class ReminderSeeder extends Seeder
{
    /**
     * Data to fill of the reminders
     * @var array[]
     */
    protected $remindersData = [
        [
            'title' => 'Нагадати за 10 днів до закінчення дії віньєтки',
            'value' => 10,
        ],
        [
            'title' => 'Нагадати за 5 днів до закінчення дії віньєтки',
            'value' => 5,
        ],
        [
            'title' => 'Нагадати за 2 днів до закінчення дії віньєтки',
            'value' => 2,
        ],
        [
            'title' => 'Нагадати в день закінчення дії віньєтки',
            'value' => 0,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reminder::factory()
            ->state(new Sequence(fn($sequence) => $this->remindersData[$sequence->index]))
            ->count(count($this->remindersData))
            ->create();
    }
}
