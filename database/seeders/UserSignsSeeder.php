<?php

namespace Database\Seeders;

use App\Models\UserSign;
use Illuminate\Database\Seeder;

class UserSignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserSign::factory()->count(25)->create();
    }
}
