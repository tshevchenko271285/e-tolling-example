<?php

namespace Database\Seeders;

use Database\Seeders\pages\ContactPageSeeder;
use Database\Seeders\pages\FaqPageSeeder;
use Database\Seeders\pages\HomePageSeeder;
use Database\Seeders\pages\MapPageSeeder;
use Database\Seeders\pages\PrivacyPolicyPageSeeder;
use Database\Seeders\pages\TermsOfUsePageSeeder;
use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            ReminderSeeder::class,
            ProductsSeeder::class,
            FaqsSeeder::class,
            FaqPageSeeder::class,
            ContactPageSeeder::class,
            MapPageSeeder::class,
            HomePageSeeder::class,
            PrivacyPolicyPageSeeder::class,
            TermsOfUsePageSeeder::class,
            UserSignsSeeder::class,
        ]);
    }
}
