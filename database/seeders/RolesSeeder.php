<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Itmaster\Manager\Models\Permission;
use Itmaster\Manager\Models\Role;

class RolesSeeder extends Seeder
{
    /**
     * Data to fill site roles
     *
     * @var array
     */
    protected array $permissions = [
        'product.index' => 'Product Index',
        'product.update' => 'Product Update',
        'product.create' => 'Product Create',
        'product.delete' => 'Product Delete',
        'product.forceDelete' => 'Hard Delete Product',
        'category.index' => 'Category Index',
        'category.update' => 'Category Update',
        'category.create' => 'Category Create',
        'category.delete' => 'Category Delete',
        'category.forceDelete' => 'Hard Delete Category',
        'order.index' => 'Order Index',
        'order.update' => 'Order Update',
        'order.create' => 'Order Create',
        'order.delete' => 'Order Delete',
        'order_item.index' => 'Order Item Index',
        'order_item.update' => 'Order Item Update',
        'order_item.create' => 'Order Item Create',
        'order_item.delete' => 'Order Item Delete',
        'user_histories.index' => 'History of clients',
        'road.index' => 'Road index',
        'road.update' => 'Road update',
        'road.create' => 'Road create',
        'road.delete' => 'Road delete',
        'faq.index' => 'FAQ index',
        'faq.update' => 'FAQ update',
        'faq.create' => 'FAQ create',
        'faq.delete' => 'FAQ delete',
        'reminder.index' => 'Reminder index',
        'reminder.update' => 'Reminder update',
        'reminder.create' => 'Reminder create',
        'reminder.delete' => 'Reminder delete',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::findByName('admin');

        foreach ($this->permissions as $key => $item) {
            $permission = Permission::firstOrCreate(
                [
                    'name' => $key,
                ],
                [
                    'name' => $key,
                    'description' => $item,
                ]
            );
            if ($role !== null && !$role->permissions()->where('permission_id', $permission->id)->exists()) {
                $role->permissions()->attach($permission->id);
            }
        }
    }
}
