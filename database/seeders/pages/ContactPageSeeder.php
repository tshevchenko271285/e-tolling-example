<?php

namespace Database\Seeders\pages;

use Itmaster\Page\Models\Page;

class ContactPageSeeder extends PageSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Contact Us',
        'slug' => 'contact',
        'user_id' => 1,
        'template_name' => 'contact',
        'visible' => Page::VISIBLE_YES,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Контакти',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Łączność',
        ],
        [
            'lang' => 'en',
            'h1' => 'Contact Us',
        ],
    ];

    /**
     * Data to fill fields of template
     *
     * @var array
     */
    protected array $fieldsData = [
        [
            'name' => 'email',
            'lang' => 'ua',
            'value' => 'info@E-Tolling.ua',
        ],
        [
            'name' => 'email',
            'lang' => 'pl',
            'value' => 'info@E-Tolling.ua',
        ],
        [
            'name' => 'email',
            'lang' => 'en',
            'value' => 'info@E-Tolling.ua',
        ],
        [
            'name' => 'phone',
            'lang' => 'ua',
            'value' => '(+380) 111 222 333',
        ],
        [
            'name' => 'phone',
            'lang' => 'pl',
            'value' => '(+380) 111 222 333',
        ],
        [
            'name' => 'phone',
            'lang' => 'en',
            'value' => '(+380) 111 222 333',
        ],
        [
            'name' => 'form_image',
            'lang' => 'ua',
            'value' => 'public/seeds/contact-form-image.png',
        ],
        [
            'name' => 'form_image',
            'lang' => 'pl',
            'value' => 'public/seeds/contact-form-image.png',
        ],
        [
            'name' => 'form_image',
            'lang' => 'en',
            'value' => 'public/seeds/contact-form-image.png',
        ],
    ];
}
