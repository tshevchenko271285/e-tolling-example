<?php

namespace Database\Seeders\pages;

use App\Http\Controllers\HomeController;
use Itmaster\Page\Models\Page;

class HomePageSeeder extends PageSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Home',
        'slug' => HomeController::HOMEPAGE_SLUG,
        'user_id' => 1,
        'template_name' => 'home',
        'visible' => Page::VISIBLE_NO,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Швидко та легко купіть електронну віньєтку',
            'description' => 'Можливість індивідуальної або групової купівлі віньєтки',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Kup winietę elektroniczną szybko i łatwo',
            'description' => 'Możliwość indywidualnego lub grupowego zakupu winiety',
        ],
        [
            'lang' => 'en',
            'h1' => 'Buy an electronic vignette quickly and easily',
            'description' => 'Possibility of individual or group purchase of a vignette',
        ],
    ];

    /**
     * Data to fill fields of template
     *
     * @var array
     */
    protected array $fieldsData = [
        [
            'name' => 'about_text',
            'lang' => 'ua',
            'value' => '
                <h2>Для чого віньєтка?</h2>
                <p>
                    Старі наклейки і коріння, які водіям доводилося зберігати у себе, канули в минуле.
                    Достатньо кількох клацань, і можна вирушати в дорогу. Нова система автоматично, по
                    ГРНЗ автомобіля дізнається, чи ви сплатили дорожній збір, і за який період часу.
                </p>
                <p>
                    Таким чином, зникає потреба у відклеюванні старого дорожнього купона,
                    після чого на склі часто залишаються сліди.
                </p>
                <p>
                    Нова електронна дорожня віньєтка не створює жодних незручностей.
                </p>
            ',
        ],
        [
            'name' => 'about_text',
            'lang' => 'pl',
            'value' => '
                <h2>Do czego służy winieta?</h2>
                <p>
                    Stare naklejki i korzenie, które kierowcy musieli mieć przy sobie, odeszły w przeszłość.
                    Wystarczy kilka kliknięć i możesz ruszać w drogę. Nowy system automatycznie wykryje, czy
                    i za jaki okres zapłaciłeś myto, na podstawie numeru rejestracyjnego samochodu.
                </p>
                <p>
                    Tym samym znika konieczność odklejania starego odcinka, po którym często pozostają ślady na szybie.
                </p>
                <p>
                    Nowa elektroniczna winieta drogowa nie powoduje żadnych niedogodności.
                </p>
            ',
        ],
        [
            'name' => 'about_text',
            'lang' => 'en',
            'value' => '
                <h2>What is the vignette for?</h2>
                <p>
                    Old stickers and roots, which drivers had to keep with themselves, sank into the past.  A few
                    clicks are enough, and you can go on your way. The new system will automatically  find out whether
                    you have paid the toll, and for what period of time, based on the car\'s registration number.
                </p>
                <p>
                    Thus, the need to peel off the old traffic coupon disappears, after which traces often
                    remain on the glass.
                </p>
                <p>
                    The new electronic road vignette does not cause any inconvenience.
                </p>
            ',
        ],
        [
            'name' => 'about_image',
            'lang' => 'ua',
            'value' => 'public/seeds/home-about.jpg',
        ],
        [
            'name' => 'about_image',
            'lang' => 'pl',
            'value' => 'public/seeds/home-about.jpg',
        ],
        [
            'name' => 'about_image',
            'lang' => 'en',
            'value' => 'public/seeds/home-about.jpg',
        ],
        [
            'name' => 'test_sign_text',
            'lang' => 'ua',
            'value' => '
                <h2>Перевірка</h2>
                <p>
                    Введіть номерний знак автомобіля, щоб дізнатися чи ваш автомобіль звільнений від плати
                    за проїзд по  платним шляхам України або перевірити дійсність електронної віньєтки
                </p>
            ',
        ],
        [
            'name' => 'test_sign_text',
            'lang' => 'pl',
            'value' => '
                <h2>Rewizja</h2>
                <p>
                    Wprowadź numer rejestracyjny swojego pojazdu, aby dowiedzieć się, czy Twój pojazd jest
                    zwolniony z opłaty drogowej do przejazdu po płatnych drogach Ukrainy lub do sprawdzenia
                    ważności elektronicznej winiety autostradowej
                </p>
            ',
        ],
        [
            'name' => 'test_sign_text',
            'lang' => 'en',
            'value' => '
                <h2>Audit</h2>
                <p>
                    Enter your vehicle license plate number to find out if your vehicle is exempt from the toll
                    for travel on the toll roads of Ukraine or to check the validity of the electronic vignette
                </p>
            ',
        ],
        [
            'name' => 'test_sign_image',
            'lang' => 'ua',
            'value' => 'public/seeds/home-test-sing.jpg',
        ],
        [
            'name' => 'test_sign_image',
            'lang' => 'pl',
            'value' => 'public/seeds/home-test-sing.jpg',
        ],
        [
            'name' => 'test_sign_image',
            'lang' => 'en',
            'value' => 'public/seeds/home-test-sing.jpg',
        ],
    ];
}
