<?php

namespace Database\Seeders\pages;

class SimpleTemplateSeeder extends PageSeeder
{
    /**
     * Data to fill fields of template
     *
     * @var array
     */
    protected array $fieldsData = [
        [
            'name' => 'text',
            'lang' => 'ua',
            'value' => '',
        ],
        [
            'name' => 'text',
            'lang' => 'pl',
            'value' => '',
        ],
        [
            'name' => 'text',
            'lang' => 'en',
            'value' => '',
        ],
    ];

    /**
     * Fills content of the page long text
     */
    public function __construct()
    {
        $this->fieldsData = array_map(function($field) {
            $textData = fake()->paragraphs(25);
            foreach ($textData as $textDatum) {
                $field['value'] .= '<p>' . $textDatum . '</p>';
            }

            return $field;
        }, $this->fieldsData);
    }
}
