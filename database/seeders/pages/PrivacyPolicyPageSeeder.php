<?php

namespace Database\Seeders\pages;

use Itmaster\Page\Models\Page;

class PrivacyPolicyPageSeeder extends SimpleTemplateSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Privacy Policy',
        'slug' => 'privacy-policy',
        'user_id' => 1,
        'template_name' => 'simple',
        'visible' => Page::VISIBLE_YES,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Політика конфіденційності',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Polityka prywatności',
        ],
        [
            'lang' => 'en',
            'h1' => 'Privacy Policy',
        ],
    ];
}
