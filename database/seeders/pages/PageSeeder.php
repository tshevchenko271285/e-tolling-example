<?php

namespace Database\Seeders\pages;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Seeder;
use Itmaster\Page\Models\Page;
use Itmaster\Page\Models\PageField;
use Itmaster\Seo\Models\SeoTranslate;

class PageSeeder extends Seeder
{
    /**
     * Page object
     *
     * @var Page
     */
    protected Page $page;

    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [];

    /**
     * Data to fill fields of template
     *
     * @var array
     */
    protected array $fieldsData = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $this->makePage();
        $this->makeSeo();
    }

    /**
     * Page creation
     *
     * @return void
     */
    protected function makePage(): void
    {
        $this->page = Page::factory($this->pageData)
            ->has($this->getFieldsFactory(), 'fields')
            ->create();
    }

    /**
     * Returns the factory with filled template fields
     *
     * @return Factory
     */
    protected function getFieldsFactory(): Factory
    {
        return PageField::factory()
            ->state(new Sequence(fn($sequence) => $this->fieldsData[$sequence->index]))
            ->count(count($this->fieldsData));
    }

    /**
     * Fills in the SEO for the created page
     *
     * @return void
     */
    protected function makeSeo(): void
    {
        SeoTranslate::factory()->state(
            new Sequence(fn ($sequence) => array_merge(
                $this->seoData[$sequence->index],
                ['seo_id' => $this->page->id]
            ))
        )->count(count($this->seoData))->create();
    }
}
