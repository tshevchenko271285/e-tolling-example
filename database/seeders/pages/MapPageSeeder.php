<?php

namespace Database\Seeders\pages;

use Itmaster\Page\Models\Page;

class MapPageSeeder extends PageSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Map',
        'slug' => 'map',
        'user_id' => 1,
        'template_name' => 'map',
        'visible' => Page::VISIBLE_YES,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Мапа платних доріг',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Mapa dróg płatnych',
        ],
        [
            'lang' => 'en',
            'h1' => 'Map of toll roads',
        ],
    ];
}
