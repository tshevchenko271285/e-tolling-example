<?php

namespace Database\Seeders\pages;

use Itmaster\Page\Models\Page;

class TermsOfUsePageSeeder extends SimpleTemplateSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Terms and conditions',
        'slug' => 'terms-of-use',
        'user_id' => 1,
        'template_name' => 'simple',
        'visible' => Page::VISIBLE_YES,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Умови використання',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Warunki korzystania',
        ],
        [
            'lang' => 'en',
            'h1' => 'Terms and conditions',
        ],
    ];
}
