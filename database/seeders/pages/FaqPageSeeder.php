<?php

namespace Database\Seeders\pages;

use Itmaster\Page\Models\Page;

class FaqPageSeeder extends PageSeeder
{
    /**
     * Data to fill the page
     *
     * @var array
     */
    protected array $pageData = [
        'name' => 'Faq',
        'slug' => 'faq',
        'user_id' => 1,
        'template_name' => 'faq',
        'visible' => Page::VISIBLE_YES,
    ];

    /**
     * Data to fill the SEO
     *
     * @var array
     */
    protected array $seoData = [
        [
            'lang' => 'ua',
            'h1' => 'Часті питання',
        ],
        [
            'lang' => 'pl',
            'h1' => 'Często Zadawane Pytania',
        ],
        [
            'lang' => 'en',
            'h1' => 'FAQ',
        ],
    ];

    /**
     * Data to fill fields of template
     *
     * @var array
     */
    protected array $fieldsData = [
        [
            'name' => 'questions_form_title',
            'lang' => 'ua',
            'value' => 'Залишились питання?',
        ],
        [
            'name' => 'questions_form_title',
            'lang' => 'pl',
            'value' => 'Czy masz jakieś pytania?',
        ],
        [
            'name' => 'questions_form_title',
            'lang' => 'en',
            'value' => 'Do you have any questions?',
        ],
        [
            'name' => 'form_image',
            'lang' => 'ua',
            'value' => 'public/seeds/faq-have-questions.jpg',
        ],
        [
            'name' => 'form_image',
            'lang' => 'pl',
            'value' => 'public/seeds/faq-have-questions.jpg',
        ],
        [
            'name' => 'form_image',
            'lang' => 'en',
            'value' => 'public/seeds/faq-have-questions.jpg',
        ],
    ];
}
