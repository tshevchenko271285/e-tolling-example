<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->changeForeignKey(true);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->changeForeignKey(false);
    }

    protected function changeForeignKey(bool $addCascadeDelete) {
        Schema::create('order_items_temp', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id');
            $table->foreignId('product_id');
            $table->string('car_country');
            $table->string('car_sign');
            $table->timestamp('started_at')->nullable();
            $table->timestamp('finished_at')->nullable();
            $table->json('product_data');
            $table->timestamps();
        });

        DB::statement("INSERT INTO order_items_temp SELECT * FROM order_items");

        Schema::dropIfExists('order_items');

        Schema::rename('order_items_temp', 'order_items');

        Schema::table('order_items', function (Blueprint $table) use ($addCascadeDelete) {
            if($addCascadeDelete) {
                $table->foreign('order_id')
                    ->references('id')->on('orders')
                    ->onDelete('cascade');
            } else {
                $table->foreign('order_id')->references('id')->on('orders');
            }

            $table->foreign('product_id')->references('id')->on('products');
        });
    }
};
