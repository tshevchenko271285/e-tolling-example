<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_histories', function (Blueprint $table) {
            $table->id();
            $table->string('visit_id');
            $table->string('page');
            $table->string('referer')->nullable();
            $table->string('parent_id')->nullable();
            $table->string('ip')->nullable();
            $table->string('auth_type')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->json('get')->nullable();
            $table->json('post')->nullable();
            $table->string('message')->nullable();
            $table->string('event_type')->nullable();
            $table->timestamps();

            $table->index('visit_id');
            $table->index('ip');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_histories');
    }
};
