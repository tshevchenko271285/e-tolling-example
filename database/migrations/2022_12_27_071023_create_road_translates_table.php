<?php

use App\Models\Exceptions\ModelException;
use App\Models\Road;
use App\Models\RoadTranslate;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        $availableLocales = config('app.available_locales', ['ua', 'pl', 'en']);

        // Creates table for translates
        Schema::create('road_translates', function (Blueprint $table) use ($availableLocales) {
            $table->id();
            $table->foreignId('road_id')->constrained()->onDelete('cascade');
            $table->enum('lang', $availableLocales)->default('ua');
            $table->string('title')->nullable();
            $table->timestamps();
        });

        // Moving title in translate
        $roads = Road::select(['id', 'title'])->get();
        $translates = [];
        foreach ($roads as $road) {
            foreach ($availableLocales as $locale) {
                $translates[] = [
                    'road_id' => $road->id,
                    'title' => $road->getRawOriginal('title'),
                    'lang' => $locale,
                ];
            }
        }
        DB::table('road_translates')->insert($translates);

        // Removes the title column from the road
        Schema::table('roads', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        // Creates title column
        Schema::table('roads', function (Blueprint $table) {
            $table->string('title')->after('number');
        });

        // Moves titles to table of roads
        $roads = RoadTranslate::select(['road_id', 'title'])
            ->where('lang', config('app.fallback_locale', 'ua'))
            ->get();


        DB::beginTransaction();
        try {
            foreach ($roads as $road) {
                Road::where('id', $road->road_id)->update(['title' => $road->title]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw new ModelException($e->getMessage(), $e->getCode(), $e);
        }

        // Removes table of road translates
        Schema::dropIfExists('road_translates');
    }
};
