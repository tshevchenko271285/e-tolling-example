<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\Product;
use App\Services\Contracts\ICartService;
use Database\Factories\VignetteFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Carbon;
use Tests\TestCase;

class CartTest extends TestCase
{
    use RefreshDatabase;

    /**
     * The tested class
     *
     * @var ICartService|mixed
     */
    protected ICartService $cartService;

    /**
     * Prepares data for testing
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->cartService = $this->app->make(ICartService::class);
    }

    /**
     * Tests getting the cart
     *
     * @return Order
     */
    public function testGetCart(): Order
    {
        $cart = $this->cartService->getCart();

        $this->assertInstanceOf(Order::class, $cart);

        return $cart;
    }

    /**
     * Tests adding items in the cart
     *
     * @return void
     */
    public function testAddItems(): void
    {
        $products = Product::factory()->count(3)->create();
        $vignettes = $this->prepareVignetteData($products);

        $this->cartService->addItems($vignettes);
        $cart = $this->cartService->getCart();

        $this->assertTrue($this->cartService->hasItems());
        $this->assertCount(count($vignettes), $cart->items);

        $badVignette = $vignettes[0];
        $badVignette['vignetteId'] = 0000;
        $this->assertCount(0, $this->cartService->addItems([$badVignette]));

        $this->assertCount(0, $this->cartService->addItems([]));

        foreach ($cart->items as $key => $item) {
            $product = $products[$key];
            $productData = $item->product_data;

            $this->assertEquals($item->product_id, $vignettes[$key]['vignetteId']);
            $this->assertEquals($item->car_country, $vignettes[$key]['country']);
            $this->assertEquals($item->car_sign, $vignettes[$key]['sign']);
            $this->assertEquals($productData->id, $product->id);
            $this->assertEquals($productData->title, $product->title);
            $this->assertEquals($productData->price, $product->price);
            $this->assertEquals($productData->term, $product->term);
        }

        $this->expectError();
        $this->cartService->addItems();
    }

    /**
     * Tests for correct date calculation
     *
     * @dataProvider datesProvider
     * @param string $startDate
     * @param string $endDate
     * @param int $term
     * @return void
     */
    public function testDates(string $startDate, string $endDate, int $term): void
    {
        $product = Product::factory(['term' => $term])->create();

        $this->cartService->addItems([[
            'country' => fake()->countryCode(),
            'sign' => fake()->bothify('??####??'),
            'vignetteId' => $product->id,
            'start' => $startDate,
        ]]);

        $cartItem = $this->cartService->getCart()->items->first();

        $startedAt = Carbon::parse($startDate)
            ->startOfDay()
            ->toDateTimeString();

        $this->assertEquals($cartItem->started_at, $startedAt);
        $this->assertEquals($cartItem->finished_at, $endDate);
    }

    /**
     * Tests removing items from the cart
     * @return void
     */
    public function testRemoveItem(): void
    {
        $this->assertFalse($this->cartService->removeItem(1));

        $products = Product::factory()->count(3)->create();
        $vignettes = $this->prepareVignetteData($products);
        $this->cartService->addItems($vignettes);

        $this->assertFalse($this->cartService->removeItem(999));

        $cart = $this->cartService->getCart();
        $countItems = $cart->items->count();
        $firstItem = $cart->items->first();

        $this->assertTrue($this->cartService->removeItem($firstItem->id));
        $this->assertCount($countItems - 1, $this->cartService->getCart()->items);

        $this->expectError();
        $this->cartService->removeItem();
    }

    /**
     * Vignette Factory
     *
     * @param Collection $products
     * @return array
     */
    protected function prepareVignetteData(Collection $products): array
    {
        $now = now();
        $vignettes = $products->map(function ($product) use ($now) {
            return [
                'country' => fake()->countryCode(),
                'sign' => fake()->bothify('??####??'),
                'vignetteId' => $product->id,
                'start' => $now,
            ];
        });

        return $vignettes->toArray();
    }

    /**
     * Dates for tests, calculation of the end time.
     *
     * @return array
     */
    public function datesProvider(): array
    {
        return [
            ['2022-12-26', '2023-01-04 23:59:59', 10],
            ['2020-02-20', '2020-02-29 23:59:59', 10],
            ['2020-05-01', '2020-05-10 23:59:59', 10],
            ['2020-06-25', '2020-07-04 23:59:59', 10],
            ['2022-12-27', '2023-01-05 23:59:59', 10],
            ['2020-12-26', '2021-01-24 23:59:59', 30],
            ['2020-02-20', '2020-03-20 23:59:59', 30],
            ['2021-02-20', '2021-03-21 23:59:59', 30],
            ['2020-03-20', '2020-04-18 23:59:59', 30],
            ['2022-04-10', '2022-05-09 23:59:59', 30],
            ['2022-05-10', '2022-06-08 23:59:59', 30],
            ['2022-12-27', '2023-01-25 23:59:59', 30],
            ['2020-02-20', '2021-02-18 23:59:59', 365],
            ['2021-02-20', '2022-02-19 23:59:59', 365],
            ['2020-02-29', '2021-02-27 23:59:59', 365],
            ['2019-03-02', '2020-02-29 23:59:59', 365],
            ['2019-05-02', '2020-04-30 23:59:59', 365],
            ['2019-09-15', '2020-09-13 23:59:59', 365],
            ['2022-12-27', '2023-12-26 23:59:59', 365],
            ['2021-03-01', '2022-02-28 23:59:59', 365],
        ];
    }
}
