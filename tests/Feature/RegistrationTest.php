<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Name of the registration route
     */
    const REGISTER_ROUTE_NAME = 'register';

    /**
     * Testing succeed registration
     * @dataProvider dataForSuccessRegistration
     * @param $data
     * @return void
     */
    public function testSuccessRegistration($data): void
    {
        $response = $this->request($data);

        $response->assertSuccessful();
        $this->assertDatabaseHas('users', ['email' => $data['email']]);
    }

    /**
     * Tests errors for request with the empty data
     * @param array $fields
     * @return void
     * @testWith [["name", "email", "phone", "country", "password"]]
     */
    public function testEmptyData(array $fields): void
    {
        $response = $this->request([]);
        $response->assertJsonValidationErrors($fields);
        $response->assertStatus(422);
    }

    /**
     * Tests errors of the validation
     * @dataProvider dataForTestsErrors
     */
    public function testFieldError($fieldName, $value): void
    {
        $this->request([$fieldName => $value])->assertJsonValidationErrorFor($fieldName);
        $this->assertDatabaseMissing('users', [$fieldName => $value]);
    }

    /**
     * Tests password confirmation errors
     * @param array $data
     * @return void
     * @testWith [{"password": "asdqwe123", "password_confirmation": "123qweasd"}]
     */
    public function testPasswordConfirmationError(array $data): void
    {
        $response = $this->request($data);
        $response->assertJsonValidationErrors('password');
        $response->assertStatus(422);
        $this->assertDatabaseMissing('users', ['password' => $data['password']]);
    }

    /**
     * Sent request to registration route
     * @param array $body
     * @return TestResponse
     */
    protected function request(array $body): TestResponse
    {
        return $this->withHeaders(['Accept' => 'application/json'])->post(route(self::REGISTER_ROUTE_NAME), $body);
    }

    /**
     * Data for testing succeed registration
     * @return array
     */
    public function dataForSuccessRegistration(): array
    {
        return [[[
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => 'qwe123qwe',
            'password_confirmation' => 'qwe123qwe',
            'phone' => '+380988303558',
            'country' => 'UA',
            'type_of_person' => User::TYPE_OF_PERSON_PRIVATE,
        ]]];
    }

    /**
     * Data for testing registration errors
     * @return array
     */
    public function dataForTestsErrors(): array
    {
        return [
            ['name', ''],
            ['name', 'a'],
            ['email', ''],
            ['email', 'not email'],
            ['phone', ''],
            ['phone', '911'],
            ['phone', 911],
            ['phone', 'not valid phone number'],
            ['country', ''],
            ['country', 'a'],
            ['password', ''],
            ['password', 'a1'],
            ['password', 'qweasdzxc'],
            ['password', 123456789],
        ];
    }
}
