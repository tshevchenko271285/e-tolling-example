<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class EncryptionTest extends TestCase
{
    use RefreshDatabase;

    protected string $phone = '911';

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testEncryptionEqual()
    {
        $user = User::factory()->create(['phone' => $this->phone]);

        $this->assertEquals($user->phone, $this->phone);
    }

    public function testGettingDataDirectlyFromDb()
    {
        $user = User::factory()->create(['phone' => $this->phone]);
        $userDB = DB::table('users')->find($user->id);

        $this->assertNotEquals($user->phone, $userDB->phone);
    }
}
