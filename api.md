# API of E-Tolling Ukraine

## Getting car signs and vignette status for it.

### Request:

To get a list of license plates, send a `GET` request to `/api/signs`.
Request headers must contain an `Accept` field with a value of `application/json`.
Other query parameters are optional.

| Field    | Type   | Default | Description                         |
|----------|--------|---------|-------------------------------------|
| page     | int    | 1       | Requested page number               |
| per_page | int    | 15      | Number of posts per pagination page |
| sign     | string | null    | Car sign                            |
| country  | string | null    | Country of car registration         |

#### Example:

``` 
/api/signs?page=1&per_page=3&sign=1
```

## Response

The response is returned in JSON format.

### Example:

```
{
    "data": [
        {
            "country": "UA",
            "car_sign": "PE5314GA",
            "active": true
        },
        {
            "country": "UA",
            "car_sign": "A1A3",
            "active": true
        },
        {
            "country": "PL",
            "car_sign": "YY5711HA",
            "active": true
        }
    ],
    "links": {
        "first": "/api/signs?page=1",
        "last": "/api/signs?page=4",
        "prev": null,
        "next": "/api/signs?page=2"
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 4,
        "links": [
            {
                "url": null,
                "label": "pagination.previous",
                "active": false
            },
            {
                "url": "/api/signs?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": "/api/signs?page=2",
                "label": "2",
                "active": false
            },
            {
                "url": "/api/signs?page=3",
                "label": "3",
                "active": false
            },
            {
                "url": "/api/signs?page=4",
                "label": "4",
                "active": false
            },
            {
                "url": "/api/signs?page=2",
                "label": "pagination.next",
                "active": false
            }
        ],
        "path": "/api/signs",
        "per_page": 3,
        "to": 3,
        "total": 10
    }
}
```

### Description of response fields:

| Field               | Type        | Description                                                                           |
|---------------------|-------------|---------------------------------------------------------------------------------------|
| data                | array       | Array with response data                                                              |
| data[].country      | string      | ISO country code                                                                      |
| data[].car_sign     | string      | Сar license plate                                                                     |
| data[].active       | bool        | Returns true if the vehicle has a valid vignette.                                     |
| links               | Object      | Contains links to navigation pages                                                    |
| links.first         | string/null | Link to first navigation page                                                         |
| links.last          | string/null | Link to last navigation page                                                          |
| links.prev          | string/null | Link to prev navigation page                                                          |
| links.next          | string/null | Link to next navigation page                                                          |
| meta                | Object      | Information about pagination                                                          |
| meta.current_page   | int         | Number of the current page, corresponds to the passed parameter `page`                |
| meta.from           | int         | Sequence number of the first rendered element on the page                             |
| meta.to             | int         | Sequence number of the last rendered element on the page                              |
| meta.last_page      | int         | Last page number                                                                      |
| meta.per_page       | int         | The number of items displayed on the page corresponds to the GET parameter `per_page` |
| meta.path           | string      | Main Request URL                                                                      |
| meta.total          | int         | The total number of available items matching the query                                |
| meta.links          | array       | Array of pagination page objects                                                      |
| meta.links[].url    | string      | Link to the page                                                                      |
| meta.links[].label  | string      | Link text                                                                             |
| meta.links[].active | bool        | true indicates the current page                                                       |

## Errors

Errors are returned in JSON format.

### Example

```
{
    "message": "The page must be a number. (and 1 more error)",
    "errors": {
        "page": [
            "The page must be a number."
        ],
        "per_page": [
            "The per page must be a number."
        ]
    }
}
```

### Description of errors fields:

| Field      | Type   | Description                                              |
|------------|--------|----------------------------------------------------------|
| message    | string | Contains the first error message                         |
| errors     | object | Contains an object with unvalidated fields               |
| errors.*[] | array  | An array of errors for the field that failed validation. |

### Translation of error messages.

To localize returned error messages, add `app-locale` header in your request. Translations are available in
English `en`, Ukrainian `ua` and Polish `pl`.
