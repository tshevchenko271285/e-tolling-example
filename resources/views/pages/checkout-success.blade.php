@extends('layouts.front')

@section('title')
    @lang('front/pagecheckout.title_success')
@endsection

@section('content')
    <div class="page-checkout">
        <div class="container">
            <div class="breadcrumbs page-checkout__breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <a href="{{ route('cart.show') }}" class="breadcrumbs__item">@lang('front/pagecart.title')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">@lang('front/pagecheckout.title')</span>
            </div>

            <h1 class="page__title page-checkout__title">@lang('front/pagecheckout.title_success')</h1>
        </div>

        <checkout :step="4"></checkout>
    </div>
@endsection
