@extends('layouts.front')

@section('title')
    @lang('front/pagecheckout.title')
@endsection

@section('content')
    <div class="page-checkout">
        <div class="container">
            <div class="breadcrumbs page-checkout__breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <a href="{{ route('cart.show') }}" class="breadcrumbs__item">@lang('front/pagecart.title')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">@lang('front/pagecheckout.title')</span>
            </div>

            <h1 class="page__title page-checkout__title">@lang('front/pagecheckout.title')</h1>
        </div>

        <checkout
            @if(session()->has(\App\Services\CartService::CHECKOUT_STEP_SESSION_KEY))
                :step="{{ session()->pull(\App\Services\CartService::CHECKOUT_STEP_SESSION_KEY) }}"
            @endif
        ></checkout>
    </div>
@endsection
