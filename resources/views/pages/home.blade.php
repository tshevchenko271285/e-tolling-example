@inject('productService', 'App\Services\ProductService')

@extends('layouts.front')

@section('title')
    {{ $seo->h1 }}
@endsection

@section('content')
    <div class="page-home">
        <div class="page-home-hero">
            <div class="container text-center">
                <h1 class="page-home-hero__title">
                    {{ $seo->h1 }}
                </h1>

                <h6 class="page-home-hero__description">
                    {{ $seo->description }}
                </h6>

                <a href="#prices" class="et-btn et-btn--dark page-home-hero__btn">
                    @lang('front/pagehome.hero.button_text')
                </a>
            </div>
        </div>

        <div id="prices" class="et-bg--gray-5 home-products">
            <div class="container">
                <div class="row">
                    @foreach($productService->getAll() as $product)
                        <div class="col-md-4">
                            <div class="home-product">
                                <div class="home-product__inner">
                                    <div class="home-product__header">
                                        <img
                                            class="img-fluid"
                                            src="/images/home-product-bg.svg"
                                            alt="{{ $seo->h1 }}"
                                        />

                                        <div class="home-product-icon home-product__icon">
                                            <div class="home-product-icon__inner">
                                                <div class="home-product-icon__left">
                                                    {{ $product->term }}<br>@lang('days')
                                                </div>

                                                <div class="home-product-icon__right">
                                                    <img src="/images/home-product-arrow.svg" alt="{{ $seo->h1 }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="home-product__body">
                                        <h3 class="home-product__title">
                                            {!! __('front/pagehome.products.title', ['count' => $product->term]) !!}
                                        </h3>

                                        <span class="home-product__price">
                                            ₴{{ $product->price }}
                                        </span>
                                    </div>

                                    <div class="home-product__actions">
                                        <buy-vignette-btn
                                            classes="w-100 et-btn et-btn--light home-product__action"
                                            button-text="@lang('front/pagehome.products.button')"
                                            vignette-id="{{ $product->id }}"
                                        ></buy-vignette-btn>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="et-two-columns d-flex align-items-center">
            <div class="et-two-columns__inner">
                <div class="et-two-columns__left"></div>

                <div class="et-two-columns__right et-two-columns__image">
                    <img src="{{ $page->field('about_image') }}" alt="{{ $seo->h1 }}">
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="et-two-columns__text page-home__about-text">
                            {!! $page->field('about_text') !!}
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="d-lg-none">
                            <img
                                class="img-fluid w-100"
                                src="{{ $page->field('about_image') }}"
                                alt="{{ $seo->h1 }}"
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="et-bg--gray-5 et-two-columns page-home__test-sign">
            <div class="et-two-columns__inner">
                <div class="et-two-columns__left et-two-columns__image">
                    <img src="{{ $page->field('test_sign_image') }}" alt="{{ $seo->h1 }}">
                </div>

                <div class="et-two-columns__right"></div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6"></div>

                    <div class="col-lg-6">
                        <div class="et-two-columns__text page-home__test-sign-text">
                            {!! $page->field('test_sign_text') !!}

                            <test-sign-form></test-sign-form>
                        </div>
                    </div>
                </div>
            </div>

            <img src="{{ $page->field('test_sign_image') }}" alt="{{ $seo->h1 }}" class="img-fluid w-100 d-lg-none">
        </div>

    </div>
@endsection
