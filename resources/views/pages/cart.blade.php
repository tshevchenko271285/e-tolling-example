@extends('layouts.front')

@section('title')
    @lang('front/pagecart.title')
@endsection

@section('content')
    <div class="page">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">@lang('front/pagecart.title')</span>
            </div>

            <h1 class="page__title">@lang('front/pagecart.title')</h1>

            <div class="">
                <cart></cart>
            </div>
        </div>
    </div>
@endsection
