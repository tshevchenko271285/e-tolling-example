@extends('layouts.front')

@section('title')
    @lang('front/profile.title')
@endsection

@section('content')
    <div class="page page-profile">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">@lang('front/profile.title')</span>
            </div>

            <h1 class="page__title">@lang('front/profile.title')</h1>

            <div class="page-profile__inner">
                <div class="page-profile__sidebar">
                    @include('front.partials.profile-sidebar')
                </div>

                <div class="page-profile__content">
                    <profile-notifications
                        init-loading-type="{{
                            $hasNewNotifications ?
                            $notificationService::TYPE_OF_LOAD_NEW :
                            $notificationService::TYPE_OF_LOAD_ALL
                        }}"
                    ></profile-notifications>
                </div>
            </div>
        </div>
    </div>
@endsection
