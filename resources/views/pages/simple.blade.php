@extends('layouts.front')

@section('title')
    {{ $seo->h1 }}
@endsection

@section('content')
    <div class="page page-simple">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">{{ $seo->h1 }}</span>
            </div>

            <h1 class="page__title">{{ $seo->h1 }}</h1>

            <div class="page-simple__text">{!! $page->field('text') !!}</div>

            <a href="{{ route('home') }}" class="et-btn et-btn--dark page-simple__back-home">
                <span class="et-btn__text">@lang('Home page')</span>
            </a>
        </div>
    </div>
@endsection
