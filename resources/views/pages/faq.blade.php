@inject('faqService', '\App\Services\FaqService')

@php
$faqs = $faqService->getAll();
@endphp

@extends('layouts.front')

@section('title')
    {{ $seo->h1 }}
@endsection

@section('content')
    <div class="page-faq">
        <div class="container">
            <div class="breadcrumbs page-faq__breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">{{ $seo->h1 }}</span>
            </div>

            <h1 class="page__title">{{ $seo->h1 }}</h1>

            <div class="row page-faq__faqs">
                <div class="col-12">

                    @foreach($faqs as $faq)

                        <div class="faq-list-item">
                            <div class="faq-list-item__inner">
                                <div class="faq-list-item__header">
                                    <h2 class="faq-list-item__question">{{ $faq->question }}</h2>

                                    <span class="faq-list-item__icon"></span>
                                </div>

                                <div class="faq-list-item__answer">
                                    <p class="mb-0">{{ $faq->answer }}</p>
                                </div>
                            </div>
                        </div>

                    @endforeach

                </div>
            </div>
        </div>

        <div class="et-bg--gray-5 et-two-columns d-flex align-items-center">
            <div class="et-two-columns__inner">
                <div class="et-two-columns__left et-two-columns__image">
                    <img src="{{ $page->field('form_image') }}" alt="{{ $seo->h1 }}">
                </div>

                <div class="et-two-columns__right"></div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6"></div>

                    <div class="col-lg-6">
                        <div class="page-faq__have-questions">
                            <contact-form
                                :subject="'{{ $seo->h1 }}'"
                                :title="'{{ $page->field('questions_form_title') }}'"
                                :url="'{{ route('contact-form.store') }}'"
                            ></contact-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script type="application/javascript">
        window.addEventListener('load', function () {
            const faqs = document.querySelectorAll('.faq-list-item');

            faqs.forEach(faq => {
                faq.querySelector('.faq-list-item__header')
                    .addEventListener('click', faqClickHandler)
            })

            function faqClickHandler() {
                this.closest('.faq-list-item').classList.toggle('faq-list-item--open');
            }
        });
    </script>

@endpush
