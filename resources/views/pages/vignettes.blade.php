@extends('layouts.front')

@section('title')
    @lang('front/pagechoosevignette.title')
@endsection

@section('content')
    <div class="page">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">@lang('front/pagechoosevignette.title')</span>
            </div>

            <h1 class="page__title">@lang('front/pagechoosevignette.title')</h1>

            <choose-vignette></choose-vignette>
        </div>
    </div>
@endsection
