@extends('layouts.front')

@section('title')
    {{ $seo->h1 }}
@endsection

@section('content')
    <div class="page-contact">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">{{ $seo->h1 }}</span>
            </div>

            <h1 class="page__title">{{ $seo->h1 }}</h1>
        </div>

        <div class="et-bg--gray-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-flex">
                        <img
                            class="d-block mt-auto mb-0 img-fluid"
                            src="{{ $page->field('form_image') }}"
                            alt="{{ $seo->h1 }}"
                        />
                    </div>

                    <div class="col-lg-6">
                        <div class="page-contact__form">
                            <div class="contacts">
                                <div class="contacts__item contacts__item--email">
                                    <h6 class="mb-0">
                                        <a href="mailto:{{ $page->field('email') }}">{{ $page->field('email') }}</a>
                                    </h6>
                                </div>

                                <div class="contacts__item contacts__item--phone">
                                    <h6 class="mb-0">
                                        <a href="tel:{{ $page->field('phone') }}">{{ $page->field('phone') }}</a>
                                    </h6>
                                </div>
                            </div>

                            <contact-form
                                :subject="'{{ $seo->h1 }}'"
                                :url="'{{ route('contact-form.store') }}'"
                            ></contact-form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script type="application/javascript">
        window.addEventListener('load', function () {
            const faqs = document.querySelectorAll('.faq-list-item');

            faqs.forEach(faq => {
                faq.querySelector('.faq-list-item__header')
                    .addEventListener('click', faqClickHandler)
            })

            function faqClickHandler() {
                this.closest('.faq-list-item').classList.toggle('faq-list-item--open');
            }
        });
    </script>

@endpush
