@extends('layouts.front')

@section('title')
    {{ $seo->h1 }}
@endsection

@section('content')
    <div class="page-map">
        <div class="container">
            <div class="breadcrumbs">
                <a href="{{ route('home') }}" class="breadcrumbs__item">@lang('Home')</a>

                <span class="breadcrumbs__separator">|</span>

                <span class="breadcrumbs__item">{{ $seo->h1 }}</span>
            </div>

            <h1 class="page__title">{{ $seo->h1 }}</h1>

            <map-of-roads></map-of-roads>

            @include('vue-templates.map-of-roads')
        </div>
    </div>
@endsection
