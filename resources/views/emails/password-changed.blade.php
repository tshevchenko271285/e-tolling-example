@component('mail::message')
    <h2>@lang('mail.profile.password_changed.text')</h2>
    <a href="{{ config('app.url') }}">{{ config('app.url') }}</a>
@endcomponent
