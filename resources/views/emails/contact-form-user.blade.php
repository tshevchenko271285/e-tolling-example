@component('mail::message')
    <h2>@lang('mail.contact_form.user_email.title'){{ $support->id }}</h2>

    <p>@lang('mail.contact_form.user_email.text')</p>
@endcomponent
