@php
    use \App\Services\MenuItem
@endphp

<nav id="sidebar">
    <div class="sidebar-header">
        <h3 class="mb-0"><a href="{{ route('admin.index') }}"> {{ __('E-Tolling') }}</a></h3>

        <button type="button" id="sidebarCollapse" class="sidebar-collapse-btn">
            <i class="fas fa-chevron-left"></i>
        </button>
    </div>

    <ul class="list-unstyled components">
        @userCan('product.index')
        <li class="{{ MenuItem::isActive(route('admin.product.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.product.index') }}">@lang('menu.menu_admin.products')</a>
        </li>
        @endUserCan

        @userCan('order.index')
        <li class="{{ MenuItem::isActive(route('admin.vignette.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.vignette.index') }}">@lang('menu.menu_admin.vignettes')</a>
        </li>
        @endUserCan

        @userCan('category.index')
        <li class="{{ MenuItem::isActive(route('admin.category.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.category.index') }}">@lang('menu.menu_admin.category_products')</a>
        </li>
        @endUserCan

        @userCan('user.index')
        <li class="{{ MenuItem::isActive(route('admin.user.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.user.index') }}">@lang('menu.menu_admin.users')</a>
        </li>
        @endUserCan

        @userCan('user_histories.index')
        <li class="{{ MenuItem::isActive(route('admin.history')) ? 'active' : '' }}">
            <a href="{{ route('admin.history') }}">@lang('menu.menu_admin.visit')</a>
        </li>
        @endUserCan

        @userCan('road.index')
        <li class="{{ MenuItem::isActive(route('admin.road.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.road.index') }}">@lang('menu.menu_admin.road')</a>
        </li>
        @endUserCan

        @userCan('faq.index')
        <li class="{{ MenuItem::isActive(route('admin.faq.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.faq.index') }}">@lang('menu.menu_admin.faq')</a>
        </li>
        @endUserCan

        @userCan('setting.index')
        <li class="{{ MenuItem::isActive(route('admin.setting.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.setting.index') }}">@lang('menu.menu_admin.settings')</a>
        </li>
        @endUserCan

        @userCan('reminder.index')
        <li class="{{ MenuItem::isActive(route('admin.reminder.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.reminder.index') }}">@lang('menu.menu_admin.reminder')</a>
        </li>
        @endUserCan

        @userCan('menu.index')
        <li class="{{ MenuItem::isActive(route('admin.menu.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.menu.index') }}">@lang('menu.menu_admin.menu')</a>
        </li>
        @endUserCan

        @userCan('snippet.index')
        <li class="{{ MenuItem::isActive(route('admin.snippet.index')) ? 'active' : '' }}">
            <a href="{{ route('admin.snippet.index') }}">@lang('menu.menu_admin.snippets')</a>
        </li>
        @endUserCan

        @foreach($packages as $item)
            @userCan($item['menu']['permission'])
            <li class="{{ MenuItem::isActive(route($item['menu']['route'])) ? 'active' : '' }}">
                <a href="{{ route($item['menu']['route']) }}">@lang($item['menu']['title'])</a>
            </li>
            @endUserCan
        @endforeach

        @stack('menu')
    </ul>
</nav>
