<script type="text/x-template" id="templateMapOfRoads">
    <div>
        <div
            id="map"
            class="map-roads page-map__map"
        ></div>

        <div
            id="mapLegend"
            ref="mapLegend"
            class="map-legend"
            style="display: none"
        >
            <div class="map-legend__inner">
                <span class="map-legend__text">
                    @lang('front/pagemap.mapLegendTollRoad')
                </span>
            </div>
        </div>

        <div v-if="roads.length" class="roads-list page-map__roads-list">
            <div class="roads-list__header">
                <h2 class="roads-list__title">@lang('front/pagemap.roadsListTitle')</h2>

                <div class="roads-list__actions">
                    <button
                        class="btn roads-list__action roads-list__action--show"
                        :class="{'roads-list__action--active' : everythingIsShown}"
                        @click="setVisibilityRoads(1)"
                    >
                        @lang('front/pagemap.roadsListShowAll')
                    </button>

                    <button
                        class="btn roads-list__action roads-list__action--hide"
                        :class="{'roads-list__action--active' : everythingIsHidden}"
                        @click="setVisibilityRoads(0)"
                    >
                        @lang('front/pagemap.roadsListHideAll')
                    </button>
                </div>
            </div>

            <div class="roads-list__items">
                <div
                    v-for="(road, index) in roads"
                    :key="index"
                    :class="{ 'roads-list-item--visible': road.visible }"
                    class="roads-list-item"
                >
                    <div
                        class="roads-list-item__inner"
                        @click="setActiveRoad(road)"
                    >
                        <div class="roads-list-item__col roads-list-item__col--number">
                            <div class="road-number">
                                <p class="road-number__inner">
                                    {# road.number #}
                                </p>
                            </div>
                        </div>

                        <p class="roads-list-item__col roads-list-item__col--title">
                            {# road.title #}
                        </p>

                        <p class="roads-list-item__col roads-list-item__col--distance">
                            {# road.distance #} {{ __('km') }}
                        </p>

                        <div
                            @click.stop="toggleVisible(road)"
                            class="roads-list-item__col roads-list-item__col--visibility"
                        ></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

@push('scripts')
    <script
        type="application/javascript"
        src="https://polyfill.io/v3/polyfill.min.js?features=default"
        defer
    ></script>

    <script
        type="application/javascript"
        src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMapOfRoad"
        defer
    ></script>

    <script type="application/javascript">
        function initMapOfRoad() {
            window.dispatchEvent(new Event('initMapOfRoad'));
        }
    </script>
@endpush
