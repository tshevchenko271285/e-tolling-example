<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    @yield('meta')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @if (!empty($seo->description))
        <meta name="description" content="{{ $seo->description }}">
    @endif
    @if (!empty($seo->robots))
        <meta name="robots" content="{{ \Itmaster\Seo\Models\Seo::getVisibilityRobots($seo->robots) }}" />
    @endif
    @if (!empty($seo->canonical))
        <link rel="canonical" href="{{ $seo->canonical }}"/>
    @endif
    @if (!empty($seo->keywords))
        <meta name="keywords" content="{{ $seo->keywords }}" />
    @endif

    <title>
        @if(View::hasSection('title'))
            @yield('title')
        @else
            {{ config('app.name') }}
        @endif
    </title>
    <!-- Styles -->
    <link href="{{ mix('css/front/app.css') }}" rel="stylesheet">

    @stack('style')
</head>
<body>

    <div
        id="app"
        @isset($viewName)class="{{ $viewName }}"@endisset
    >

        @include('front.header')

        <main>
            @yield('content')
        </main>

        @include('front.footer')

        @guest
            <modal-register></modal-register>
            <modal-login></modal-login>
            <modal-forgot-password></modal-forgot-password>
        @endguest

    </div>

    @auth
        <script type="text/javascript">
            window.userID = {{ auth()->user()->id }};
        </script>
    @endauth

    <!-- Scripts -->
    @empty($noRoutes)
        <script src="{{ route('translations') }}" type="text/javascript" defer></script>
    @endempty
    <script src="{{ mix('js/front/app.js') }}" type="text/javascript" defer></script>

    @stack('scripts')
</body>
</html>
