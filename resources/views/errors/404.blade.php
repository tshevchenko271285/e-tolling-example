@extends('layouts.front')

@section('title')
    @lang('front/page_error.404.title')
@endsection

@section('content')
    <div class="page page-error">
        <div class="container">
            <hr class="my-0" style="border: 1px solid #000000;">

            <div class="row page-error__content">
                <div class="col-lg-6">
                    <h1 class="page-error__title">
                        @lang('front/page_error.404.code')

                        <img
                            src="/images/error-icon.svg"
                            alt="@lang('front/page_error.404.title')"
                            class="page-error__icon"
                        />
                    </h1>
                </div>

                <div class="col-lg-6">
                    <h2 class="page-error__text">
                        @lang('front/page_error.404.title')
                    </h2>

                    <a href="/{{ \App::getLocale() }}" class="et-btn et-btn--dark page-error__button">
                        <span class="et-btn__text">@lang('front/page_error.404.button')</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
