@php
    use \App\Services\MenuItem;
@endphp

<div class="profile-sidebar">
    <nav class="profile-sidebar__inner">
        <ul class="profile-sidebar__items">
            <li
                @class([
                    'profile-sidebar__item',
                    'profile-sidebar__item--active' => MenuItem::isActive(route('profile.show'), false),
                ])
            >
                <a class="profile-sidebar__link" href="{{ route('profile.show') }}">
                    @lang('front/profile.sidebar.profile')
                </a>
            </li>

            <li
                @class([
                    'profile-sidebar__item',
                    'profile-sidebar__item--active' => MenuItem::isActive(route('profile.vignettes')),
                    'profile-sidebar__item--reminder' => $hasVignettesReminder,
                ])
            >
                <a class="profile-sidebar__link" href="{{ route('profile.vignettes') }}">
                    @lang('front/profile.sidebar.vignettes')
                </a>
            </li>

            <li
                @class([
                    'profile-sidebar__item',
                    'profile-sidebar__item--active' => MenuItem::isActive(route('profile.signs')),
                ])
            >
                <a class="profile-sidebar__link" href="{{ route('profile.signs') }}">
                    @lang('front/profile.sidebar.numbers')
                </a>
            </li>

            <li
                @class([
                    'profile-sidebar__item',
                    'profile-sidebar__item--active' => MenuItem::isActive(route('profile.notifications')),
                    'profile-sidebar__item--reminder' => $hasNewNotifications,
                ])
            >
                <a class="profile-sidebar__link" href="{{ route('profile.notifications') }}">
                    @lang('front/profile.sidebar.notifications')
                </a>
            </li>
        </ul>

        <hr>

        <a href="{{ route('logout') }}" class="btn profile-sidebar__logout">
            @lang('front/profile.sidebar.exit')

            <img
                src="/images/profile-exit-icon.svg"
                alt="@lang('front/profile.sidebar.exit')"
            />
        </a>
    </nav>
</div>
