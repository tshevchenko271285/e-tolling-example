<footer class="et-footer">
    <div class="container">
        @widget(
            'menu',
            [
                'code' => 'footer-'.\App::getLocale(),
                'view' => 'widgets.menu.footer-index',
                'class' => '',
                'side' => 'left',
            ]
        )

        <hr>

        <div class="et-footer-bottom">
            <a href="/" class="et-footer-bottom__logo">
                <img src="/images/logos/2.svg" alt="E-Tolling Ukraine" width="60" height="60" class="img-fluid">
            </a>

            <p class="et-footer-bottom__copyright-text">
                @lang('front/footer.copyright_text')
            </p>

            <p class="et-footer-bottom__privacy_policy">
                <a href="{{ \App\Facades\Setting::get('footer_privacy_policy_link') }}">
                    @lang('front/footer.privacy_policy')
                </a>
            </p>
        </div>
    </div>
</footer>
