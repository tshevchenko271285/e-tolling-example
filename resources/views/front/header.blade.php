<header class="et-header">
    <div class="et-header__empty-space"></div>
    @widget(
        'menu',
        [
            'code' => 'header-'.\App::getLocale(),
            'view' => 'widgets.menu.header-index',
            'class' => 'navbar-expand-xl',
            'side' => 'left',
        ]
    )
</header>
