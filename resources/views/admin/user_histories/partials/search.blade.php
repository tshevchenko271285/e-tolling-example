@php
    use App\Services\HistoryService;
    use Carbon\Carbon;
@endphp

<form
    class="form-inline d-flex justify-content-end flex-column flex-lg-row align-items-end"
    method="post"
    action="{{ route('admin.history.search') }}"
>
    @csrf

    <div class="form-group my-2">
        <label class="mr-2">{{ trans('user_histories.search.ip') }}</label>
        <input type="text" name="ip" class="form-control pull-right mr-lg-2">
    </div>

    <div class="form-group my-2">
        <label class="mr-2">{{ trans('user_histories.search.dates') }}</label>
        <input
            type="text"
            name="dates"
            class="
                form-control pull-right mr-lg-2
                @error('start_date') is-invalid @enderror
                @error('end_date') is-invalid @enderror
            "
        >
    </div>

    <input type="hidden" name="start_date" value="{{ old('start_date') }}"/>
    <input type="hidden" name="end_date" value="{{ old('end_date') }}"/>

    <button type="submit" class="btn btn-primary my-2">{{ trans('user_histories.search.search') }}</button>
    @if ($errors->any())
        <div class="w-100 text-right text-danger small py-2">{{ $errors->first() }}</div>
    @endif
</form>

@push('style')
    <link rel="stylesheet" type="text/css" href="/css/admin/daterangepicker.css"/>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/daterangepicker.js" defer></script>
    <script type="text/javascript" defer>
        window.addEventListener('load', function () {
            let startDate;
            let endDate;
            @if(old('start_date'))
                startDate = '{{ old('start_date') }}';
            @elseif(request()->has('start_date'))
                startDate = '{{ request()->input('start_date') }}';
            @else
                startDate = '{{ Carbon::now()->format('m/d/Y') }}';
            @endif

            @if(old('end_date'))
                endDate = '{{ old('end_date') }}';
            @elseif(request()->has('end_date'))
                endDate = '{{ request()->input('end_date') }}';
            @else
                endDate = '{{ Carbon::now()->format('m/d/Y') }}';
            @endif

            const $picker = $('input[name="dates"]').daterangepicker({
                startDate: startDate,
                endDate: endDate,
                maxDate: moment(),
                autoApply: true,
                locale: {
                    "format": '{{ HistoryService::SEARCH_DATE_FORMAT }}',
                }
            }, function(start, end, label) {
                $startField.val(start.format('{{ HistoryService::SEARCH_DATE_FORMAT }}'));
                $endField.val(end.format('{{ HistoryService::SEARCH_DATE_FORMAT }}'));
            });

            const $form = $picker.closest('form');
            const $startField = $form.find('input[name="start_date"]');
            const $endField = $form.find('input[name="end_date"]');

            $startField.val(startDate);
            $endField.val(endDate);
        });
    </script>
@endpush
