@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <a href="{{ route('admin.history.show', $history->visit_id) }}" class="btn btn-primary pull-right">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('common.back') }}
            </a>
        </div>

        <table class="table table-stripped">
            <tbody>
                <tr>
                    <th>{{ trans('user_histories.details.event_type') }}:</th>
                    <td>{{ $history->event_type }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.visit_id') }}:</th>
                    <td>{{ $history->visit_id }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.date') }}:</th>
                    <td>{{ $history->created_at }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.page') }}:</th>
                    <td>{{ $history->page }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.referer') }}:</th>
                    <td>{{ $history->referer }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.ip') }}:</th>
                    <td>{{ $history->ip }}</td>
                </tr>
                <tr>
                    <th>{{ trans('user_histories.details.auth_type') }}:</th>
                    <td>{{ $history->auth_type }}</td>
                </tr>
                @if($history->user)
                    <tr>
                        <th>{{ trans('user_histories.details.user') }}:</th>
                        <td>
                            {{ $history->user->name }}<br />
                            {{ $history->user->phone }}<br />
                            {{ $history->user->email }}
                        </td>
                    </tr>
                @endif

                @if($history->event_type === \App\Services\HistoryService::EVENT_TYPE_AUTO)
                    <tr>
                        <th>{{ $history->method }}:</th>
                        <td>{{ \App\Models\UserHistory::METHOD_GET === $history->method ? $history->get : $history->post}}</td>
                    </tr>
                @else
                    <tr>
                        <th>{{ trans('user_histories.details.message') }}:</th>
                        <td>{{ $history->message }}</td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>

@endsection
