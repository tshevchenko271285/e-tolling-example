@extends('layouts.admin.app')

@section('title')
    {{ trans('user_histories.title') }}
@endsection

@section('content')

    <div class="container">

        <div>
            @include('admin.user_histories.partials.search')
        </div>

        <table class="table table-stripped user-histories-table">
            <thead>
            <tr>
                <th>
                    {{ trans('user_histories.id') }}
                </th>
                <th class="text-right">
                    {{ trans('user_histories.date') }}
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($histories as $history)
                <tr>
                    <td style="width: 280px;">
                        <a href="{{ route('admin.history.show', $history->visit_id) }}">
                            {{ $history->visit_id }}
                        </a>
                    </td>
                    <td class="text-right">{{ $history->created_at }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3" class="text-center">
                        <h2>{{ trans('common.no_data') }}</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="2">
                    {{ $histories->links() }}
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
