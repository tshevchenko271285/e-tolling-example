@extends('layouts.admin.app')

@section('title')
    {{ trans('user_histories.title') }}
@endsection

@section('content')

    <div class="container">

        <div class="d-flex justify-content-between align-items-center mb-2 mr-2">
            <a href="{{ route('admin.history') }}" class="btn btn-primary pull-right">
                <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ trans('common.back') }}
            </a>

            <div>
                @include('admin.user_histories.partials.search')
            </div>
        </div>

        <table class="table table-stripped user-histories-table">
            <thead>
            <tr>
                <th style="max-width: 400px;">
                    {{ trans('user_histories.page') }}
                </th>
                <th>
                    {{ trans('user_histories.date') }}
                </th>
                <th>
                    {{ trans('user_histories.method') }}
                </th>
                <th>
                    {{ trans('user_histories.ip') }}
                </th>
                <th>
                    {{ trans('user_histories.user') }}
                </th>
                <th>
                    {{ trans('user_histories.event_type') }}
                </th>
            </tr>
            </thead>
            <tbody>
            @forelse($histories as $history)
                <tr>
                    <td style="max-width: 400px; word-break: break-all;">
                        <a href="{{ route('admin.history.details', $history) }}">
                            {{ $history->page }}
                        </a>
                    </td>
                    <td>{{ $history->created_at }}</td>
                    <td>{{ $history->method }}</td>
                    <td>{{ $history->ip }}</td>
                    <td>{{ $history->user?->name }}</td>
                    <td>{{ $history->event_type }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="7" class="text-center">
                        <h2>{{ trans('common.no_data') }}</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="6">
                    {{ $histories->links() }}
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
@endsection
