<div class="container mb-5">
    @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('admin.vignette.index') }}" method="GET">
        @if(request()->has('manager'))
            <input type="hidden" value="{{ request()->get('manager') }}" name="manager">
        @endif

        @if(request()->has('sort'))
            <input type="hidden" value="{{ request()->get('sort') }}" name="sort">
        @endif

        @if(request()->has('order'))
            <input type="hidden" value="{{ request()->get('order') }}" name="order">
        @endif

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="searchField">@lang('vignette.filter.search_label')</label>

                    <input
                        type="text"
                        name="search"
                        class="form-control"
                        id="searchField"
                        placeholder="@lang('vignette.filter.search_label')"
                        value="{{ request()->get('search') }}"
                        autocomplete="off"
                    />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="dateField">@lang('vignette.filter.date_label')</label>

                    <input
                        id="dateField"
                        type="text"
                        name="date"
                        class="form-control pull-right mr-lg-2"
                        value="{{ request()->get('date') }}"
                        autocomplete="off"
                    />
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="termField">@lang('vignette.filter.term_label')</label>

                    <select class="form-control" id="termField" name="term">
                        <option value>@lang('vignette.filter.term_label')</option>

                        @foreach($products as $product)
                            <option
                                value="{{ $product->term }}"
                                @selected(request()->get('term') == $product->term)
                            >
                                {{ $product->term }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        <div class="d-flex justify-content-between">
            <a
                href="{{ route('admin.vignette.index') }}{{ request()->has('manager') ? '?manager=1' : '' }}"
                class="btn btn-outline-primary"
                type="button"
            >
                @lang('vignette.filter.clear')
            </a>

            <button class="btn btn-primary" type="submit">
                @lang('vignette.filter.find')
            </button>
        </div>
    </form>
</div>

@push('style')
    <link rel="stylesheet" type="text/css" href="/css/admin/daterangepicker.css"/>
@endpush

@push('scripts')
    <script type="text/javascript" src="/js/daterangepicker.js" defer></script>

    <script type="text/javascript" defer>
        window.addEventListener('load', function () {
            const $picker = $('input[name="date"]').daterangepicker({
                autoApply: true,
                autoUpdateInput: false,
                singleDatePicker: true,
                locale: {
                    "format": 'DD.MM.YYYY',
                }
            });

            $picker.on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD.MM.YYYY'));
            });

            $picker.on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>
@endpush
