@php
    use \App\Services\MenuItem;
@endphp

<div class="container">
    <ul class="nav nav-tabs mb-5">
        <li class="nav-item">
            <a
                class="nav-link {{ MenuItem::isActive(route('admin.vignette.index'), false) && !request()->has('manager') ? 'active' : '' }}"
                href="{{ route('admin.vignette.index') }}"
            >
                {{ trans('vignette.tabs.all') }}
            </a>
        </li>

        <li class="nav-item">
            <a
                class="nav-link {{ MenuItem::isActive(route('admin.vignette.index')) && request()->has('manager') ? 'active' : '' }}"
                href="{{ route('admin.vignette.index') }}?manager=1"
            >
                {{ trans('vignette.tabs.generated') }}
            </a>
        </li>

        <li class="nav-item">
            <a
                class="nav-link {{ MenuItem::isActive(route('admin.vignette.create')) ? 'active' : '' }}"
                href="{{ route('admin.vignette.create') }}"
            >
                {{ trans('vignette.tabs.add') }}
            </a>
        </li>
    </ul>
</div>
