@extends('layouts.admin.app')

@section('title')
    {{ trans('vignette.title') }}
@endsection

@section('content')

    @include('admin.vignette.partials.tabs')

    <div class="container">
        <cart-create></cart-create>
    </div>

@endsection
