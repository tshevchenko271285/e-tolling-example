@extends('layouts.admin.app')

@section('title')
    {{ trans('vignette.title') }}
@endsection

@section('content')

    @include('admin.vignette.partials.tabs')

    @include('admin.vignette.partials.filter')

    <div class="container">
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th style="white-space: nowrap;">
                        <a href="{{ \App\Services\UrlService::getSortingUrl(route('admin.vignette.index'), 'id') }}">
                            {{ trans('vignette.id') }} <i class="fas fa-sort"></i>
                        </a>

                    </th>

                    <th>{{ trans('vignette.car_country') }}</th>

                    <th>{{ trans('vignette.car_sign') }}</th>

                    <th style="white-space: nowrap;">
                        <a href="{{ \App\Services\UrlService::getSortingUrl(route('admin.vignette.index'), 'started_at') }}">
                            {{ trans('vignette.date_start') }} <i class="fas fa-sort"></i>
                        </a>
                    </th>

                    <th style="white-space: nowrap;">
                        <a href="{{ \App\Services\UrlService::getSortingUrl(route('admin.vignette.index'), 'finished_at') }}">
                            {{ trans('vignette.date_end') }} <i class="fas fa-sort"></i>
                        </a>
                    </th>

                    <th>{{ trans('vignette.term') }}</th>

                    <th>{{ trans('vignette.price') }}</th>

                    <th>{{ trans('vignette.user') }}</th>

                    @if(request()->has('manager'))
                        <th>{{ trans('vignette.manager') }}</th>
                    @endif
                </tr>
            </thead>

            <tbody>
            @forelse($vignettes as $vignette)
                <tr>
                    <td>{{ $vignette->id }}</td>

                    <td>{{ $vignette->car_country }}</td>

                    <td>{{ $vignette->car_sign }}</td>

                    <td>{{ $vignette->started_at->format('d.m.Y') }}</td>

                    <td>{{ $vignette->finished_at->format('d.m.Y') }}</td>

                    <td>{{ $vignette->product_data->term }}</td>

                    <td>{{ $vignette->product_data->price }} {{ trans('common.currency') }}</td>

                    <td>{{ $vignette->order->email }}<br />{{ $vignette->order->phone }}</td>

                    @if(request()->has('manager'))
                        <td>{{ $vignette->order->manager->name }} {{ $vignette->order->manager->surname }}</td>
                    @endif
                </tr>
            @empty
                <tr>
                    <td colspan="{{ request()->has('manager') ? 8 : 7 }}" class="text-center">
                        <h2>{{ trans('common.no_data') }}</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>

            <tfoot>
                <tr>
                    <td colspan="{{ request()->has('manager') ? 8 : 7 }}">
                        {{ $vignettes->links() }}
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>

@endsection
