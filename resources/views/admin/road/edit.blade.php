@extends('layouts.admin.app')

@section('content')

    <div class="container">

        @if(!empty($road->coordinates))
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <div id="map" class="map"></div>
                </div>
            </div>
        @endif

        <form
            class="form-horizontal"
            action="{{route('admin.road.update', $road)}}"
            method="post"
            enctype="multipart/form-data"
        >
            @csrf

            @method('PATCH')

            @include('admin.road.partials.form', ['road' => $road])

            <hr/>

            <a href="{{ route('admin.road.index') }}" class="btn btn-default">
                {{ trans('common.cancel') }}
            </a>

            <input type="submit" class="btn btn-primary" value="{{ trans('common.save') }}">
        </form>
    </div>

@endsection

@if(!empty($road->coordinates))
    @push('scripts')
        @include('test-example.partials.map-script', ['roads' => collect([$road])])
    @endpush
@endif
