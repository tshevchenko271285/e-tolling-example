@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<ul class="nav nav-pills my-3 justify-content-end" id="pills-tab" role="tablist">
    @foreach($languages as $key => $language)
        <li class="nav-item">
            <a class="nav-link {{ $key == 0 ? 'active' : 0 }}"
               id="pills-home-tab-{{ $language }}"
               data-toggle="pill"
               href="#pills-{{ $language }}"
               role="tab"
            >
                {{ mb_strtoupper($language) }}
            </a>
        </li>
    @endforeach
</ul>

<div class="tab-content">
    @foreach($languages as $key => $language)
        <div class="tab-pane {{ $key === 0 ? 'active' : '' }}" id="pills-{{ $language }}" role="tabpanel">
            <div class="form-group">
                <label for="fieldTitle{{ $language }}">{{ trans('road.title') }}</label>

                <input
                    id="fieldTitle{{ $language }}"
                    type="text"
                    class="form-control"
                    name="title[{{ $language }}]"
                    placeholder="{{ trans('road.title') }}"
                    value="{{ old('title.' . $language, !empty($road) ? $road->getTranslate($language)?->title : '') }}"
                />
            </div>
        </div>
    @endforeach
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="fieldNumber">{{ trans('road.number') }}</label>

            <input
                id="fieldNumber"
                type="text"
                class="form-control"
                name="number"
                placeholder="{{ trans('road.number') }}"
                value="{{ old('number', !empty($road) ? $road->number : "") }}"
            />
        </div>
    </div>

    <div class="col-6">
        <div class="form-group">
            <label for="fieldDistance">{{ trans('road.distance') }}</label>
            <input
                id="fieldDistance"
                type="number"
                class="form-control"
                name="distance"
                placeholder="{{ trans('road.distance') }}"
                value="{{ old('distance', !empty($road) ? $road->distance : "") }}"
            />
        </div>
    </div>
</div>

<div class="form-group">
    <label for="fieldFile">{{ trans('road.file') }}</label>

    <input
        id="fieldFile"
        type="file"
        name="coordinates"
        class="form-control-file @error('coordinates') is-invalid @enderror"
    />

    @error('coordinates')
        <div class="invalid-feedback">{{ $message }}</div>
    @enderror
</div>
