@extends('layouts.admin.app')

@section('title')
    {{ trans('road.pageTitle') }}
@endsection

@section('content')

    <div class="container">
        @userCan('product.create')
            <a href="{{route('admin.road.create')}}" class="btn btn-primary pull-right">
                <i class="fas fa-plus"></i> {{ trans('common.create') }}
            </a>
        @endUserCan

        <table class="table table-stripped" aria-describedby="Categories table">
            <thead>
                <tr>
                    <th>
                        {{ trans('road.id') }}
                    </th>
                    <th>
                        {{ trans('road.number') }}
                    </th>
                    <th>
                        {{ trans('road.title') }}
                    </th>
                    <th>
                        {{ trans('road.distance') }}
                    </th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            @forelse($roads as $road)
                <tr>
                    <td>{{ $road->id }}</td>
                    <td>{{ $road->number }}</td>
                    <td>{{ $road->title }}</td>
                    <td>{{ $road->distance }}</td>
                    <td class="text-right">
                        @userCan('road.update')
                            <a
                                href="{{ route('admin.road.edit', $road) }}"
                                class="btn btn-default d-inline-block"
                                title="{{ trans('common.edit') }}"
                            >
                                <i class="fas fa-edit"></i>
                            </a>
                        @endUserCan

                        @userCan('road.delete')
                            <form
                                onsubmit="if(confirm('Delete?')) { return true;} else { return false; }"
                                action="{{ route('admin.road.destroy', $road) }}"
                                method="post"
                                class="d-inline-block"
                            >
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-default" type="submit" title="{{ trans('common.delete') }}">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </form>
                        @endUserCan
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5" class="text-center">
                        <h2>{{ trans('common.no_data') }}</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5">
                    {{ $roads->links() }}
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
