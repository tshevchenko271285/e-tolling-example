@extends('layouts.admin.app')

@section('title')
    {{ trans('reminder.pageTitle') }}
@endsection

@section('content')

    <div class="container">
        @userCan('reminder.create')
        <a href="{{ route('admin.reminder.create') }}" class="btn btn-primary pull-right mb-3">
            <i class="fas fa-plus"></i> {{ trans('common.create') }}
        </a>
        @endUserCan

        <table class="table table-stripped" aria-describedby="Categories table">
            <thead>
            <tr>
                <th>
                    {{ trans('reminder.id') }}
                </th>
                <th>
                    {{ trans('reminder.title') }}
                </th>
                <th>
                    {{ trans('reminder.value') }}
                </th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($reminders as $reminder)
                <tr>
                    <td>{{ $reminder->id }}</td>
                    <td>{{ $reminder->title }}</td>
                    <td>{{ $reminder->value }}</td>
                    <td class="text-right">
                        @userCan('reminder.update')
                        <a
                            href="{{ route('admin.reminder.edit', $reminder) }}"
                            class="btn btn-default d-inline-block"
                            title="{{ trans('common.edit') }}"
                        >
                            <i class="fas fa-edit"></i>
                        </a>
                        @endUserCan

                        @userCan('reminder.delete')
                        <form
                            onsubmit="if(confirm('Delete?')) { return true;} else { return false; }"
                            action="{{ route('admin.reminder.destroy', $reminder) }}"
                            method="post"
                            class="d-inline-block"
                        >
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-default" type="submit" title="{{ trans('common.delete') }}">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </form>
                        @endUserCan
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4" class="text-center">
                        <h2>{{ trans('common.no_data') }}</h2>
                    </td>
                </tr>
            @endforelse
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4">
                    {{ $reminders->links() }}
                </td>
            </tr>
            </tfoot>
        </table>
    </div>

@endsection
