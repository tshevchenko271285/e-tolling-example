@if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="row">
    <div class="col-md-8">
        <div class="form-group">
            <label for="fieldNumber">{{ trans('reminder.title') }}</label>

            <input
                id="fieldTitle"
                type="text"
                class="form-control"
                name="title"
                placeholder="{{ trans('reminder.title') }}"
                value="{{ old('title', !empty($reminder) ? $reminder->title : '') }}"
            />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label for="fieldValue">{{ trans('reminder.value') }}</label>

            <input
                id="fieldValue"
                type="number"
                class="form-control"
                name="value"
                placeholder="{{ trans('reminder.value') }}"
                value="{{ old('value', !empty($reminder) ? $reminder->value : "") }}"
            />
        </div>
    </div>
</div>
