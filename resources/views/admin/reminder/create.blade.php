@extends('layouts.admin.app')

@section('title')
    {{ trans('reminder.pageTitle') }}
@endsection

@section('content')

    <div class="container">
        <form
            class="form-horizontal"
            action="{{route('admin.reminder.store')}}"
            method="post"
            enctype="multipart/form-data"
        >
            @csrf

            @include('admin.reminder.partials.form')

            <hr/>

            <a href="{{ route('admin.reminder.index') }}" class="btn btn-default">
                {{ trans('common.cancel') }}
            </a>

            <input
                type="submit"
                class="btn btn-primary"
                value="{{ trans('common.save') }}"
            />
        </form>
    </div>

@endsection
