<h1>Example page</h1>
<hr>
<h2>Text field example:</h2>
<p>{{ $page->field('fieldTextExample') }}</p>
<hr>

<h2>Checkbox field example:</h2>
<p>The checkbox {{ $page->field('fieldCheckboxExample') ? 'checked' : 'unchecked'}}</p>
<hr>

<h2>File field example:</h2>
<p>
    <a href="{{ $page->field('fieldFileExample') }}" target="_blank">Try the loaded file</a>
</p>
<hr>

<h2>Editor field example:</h2>
<div>{!! $page->field('fieldEditorExample') !!}</div>
<hr>


