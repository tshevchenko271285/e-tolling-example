import Vue from 'vue';
import Vuex from 'vuex';

import countries from '@front-resources-js/store/modules/countries/index';
import products from '@front-resources-js/store/modules/products/index';
import { chooseVignettes } from '@front-resources-js/store/modules/choose-vignettes/index';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        countries,
        products,
        chooseVignettes,
    }
});

export default store;
