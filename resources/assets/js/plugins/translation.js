import getLodash from "lodash/get";
import eachRightLodash from "lodash/eachRight";
import replaceLodash from "lodash/replace";

/**
 * Returns the translation of a string from language files Laravel
 */
export default {
    install(Vue, options) {
        Vue.prototype.trans = (string, args) => {
            let value = getLodash(window.i18n, string);

            eachRightLodash(args, (paramVal, paramKey) => {
                value = replaceLodash(value, `:${paramKey}`, paramVal);
            });

            return value ? value : string;
        };
    }
};
