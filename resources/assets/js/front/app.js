/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import store from './store/index';
import VueTippy, { TippyComponent } from 'vue-tippy';
import Translations from '@resources-js/plugins/translation.js';

require('./bootstrap');

window.Vue = require('vue');
window.Vue.use(VueTippy);
window.Vue.use(Translations);

Vue.component('v-select', require('vue-select').default);
Vue.component('v-datepicker', require('vue2-datepicker').default);
Vue.component('tippy', TippyComponent);

Vue.component('map-of-roads', require('./components/map-of-roads/MapOfRoadsComponent.vue'));
Vue.component('contact-form', require('./components/contact-form/ContactForm.vue'));
Vue.component('modal-register', require('./components/modal-register/ModalRegister.vue'));
Vue.component('modal-login', require('./components/modal-login/ModalLogin.vue'));
Vue.component('modal-forgot-password', require('./components/modal-password-forgot/ModalPasswordForgot.vue'));
Vue.component('password-reset', require('./components/password-reset/PasswordReset.vue'));
Vue.component('test-sign-form', require('./components/test-sign-form/TestSignForm.vue'));
Vue.component('choose-vignette', require('./components/choose-vignette/ChooseVignette.vue'));
Vue.component('buy-vignette-btn', require('./components/buy-vignette-btn/BuyVignetteBtn.vue'));
Vue.component('sign-field', require('./components/sign-field/SignField.vue'));
Vue.component('phone-field', require('./components/phone-field/PhoneField.vue'));
Vue.component('cart', require('./components/cart/Cart.vue'));
Vue.component('checkout', require('./components/checkout/Checkout.vue'));
Vue.component('profile-form', require('./components/profile/ProfileForm.vue'));
Vue.component('my-vignettes', require('./components/profile/MyVignettes.vue'));
Vue.component('profile-signs', require('./components/profile/ProfileSigns.vue'));
Vue.component('profile-notifications', require('./components/profile/ProfileNotifications.vue'));
Vue.component('et-countries', require('./components/countries/Countries.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const app = new Vue({
    el: '#app',
    store,

    mounted() {
        this.$store.dispatch('cart/checkCart');
    },
});

/**
 * Toggle main mobile menu
 */
window.addEventListener('load', function() {
    const button = document.querySelector('#ETMobileMenuToggle');
    if(button) {
        button.addEventListener('click', () => document.body.classList.toggle('et--menu-opened'));
    }
});
