/**
 * The date format for uses in the view
 * @type {string}
 */
export const DATE_FORMAT = 'DD.MM.YYYY';

/**
 * The vignette model
 */
const Vignette = function(params) {
    const now = moment().endOf('day');
    const start = 'start' in params ? moment.parseZone(params.start).local(true).startOf('day') : '';
    const end = 'end' in params ? moment.parseZone(params.end).local(true).endOf('day') : '';
    const reminderDate = end.clone().subtract(params.change_status, 'days');
    let status = '';

    if (now.isAfter(end)) {
        status = this.STATUS_NOT_ACTIVE;
    } else if (now.isSameOrAfter(reminderDate)) {
        status = this.STATUS_REMINDER;
    } else {
        status = this.STATUS_ACTIVE;
    }

    return {
        id: 'id' in params ? params.id : '',
        term: 'term' in params ? params.term : '',
        start: start.format(DATE_FORMAT),
        end: end.format(DATE_FORMAT),
        status: status,
        sign: 'sign' in params ? params.sign : '',
        change_status: 'change_status' in params ? params.change_status : 0,
        categoryTitle: 'categoryTitle' in params ? params.categoryTitle : '',
        categoryImage: 'categoryImage' in params ? params.categoryImage : '',
    };
}

/**
 * Status for the active vignettes
 * @type {string}
 */
Vignette.prototype.STATUS_ACTIVE = 'active';

/**
 * Status for the no-active vignettes
 * @type {string}
 */
Vignette.prototype.STATUS_NOT_ACTIVE = 'not_active';

/**
 * The status of the vignettes is ending
 * @type {string}
 */
Vignette.prototype.STATUS_REMINDER = 'reminder';

export default Vignette;
