import Vue from 'vue';
import Vuex from 'vuex';
import roads from './modules/roads/index';
import users from './modules/users/index';
import modals from './modules/modals/index';
import countries from './modules/countries/index';
import products from './modules/products/index';
import { chooseVignettes } from './modules/choose-vignettes/index';
import userSigns from './modules/user-signs/index';
import cart from './modules/cart/index';
import checkout from './modules/checkout/index';
import {myVignettes} from './modules/my-vignettes/index';
import {notifications} from './modules/notifications/index';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        roads,
        users,
        modals,
        countries,
        products,
        chooseVignettes,
        userSigns,
        cart,
        checkout,
        myVignettes,
        notifications,
    }
});

export default store;
