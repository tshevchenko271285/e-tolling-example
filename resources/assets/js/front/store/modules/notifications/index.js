/**
 * The vignette notification type
 * @type {string}
 */
const NOTIFICATION_TYPE_OF_VIGNETTE = 'App\\Notifications\\VignetteReminderNotification';

/**
 * Loading type - all.
 * @type {string}
 */
const TYPE_OF_LOAD_ALL = 'all';

/**
 * Loading type - new.
 * @type {string}
 */
const TYPE_OF_LOAD_NEW = 'new';

/**
 * The storage of notifications
 */
const notifications = {
    namespaced: true,

    state: {
        paginateNotifications: null,
        page: 1,
        pagination: null,
        loading: false,
        typeOfLoading: TYPE_OF_LOAD_NEW,
    },

    getters: {
        /**
         * Returns User notifications from storage
         * @param state
         */
        getPaginateNotifications(state) {
            return state.paginateNotifications;
        },

        /**
         * Returns the pagination object for the user notifications
         * @param state
         */
        getPagination(state) {
            return state.pagination;
        },

        /**
         * Returns the loading status
         * @param state
         */
        getLoading(state) {
            return state.loading;
        },

        /**
         * Returns type of lading
         * @param state
         * @returns {string}
         */
        getTypeOfLoading(state) {
            return state.typeOfLoading;
        },
    },

    mutations: {
        /**
         * Saving User notifications in the storage
         * @param state
         * @param paginateNotifications
         */
        setPaginateNotifications(state, paginateNotifications) {
            state.paginateNotifications = paginateNotifications;
        },

        /**
         * Sets the pagination object of the notifications
         * @param state
         * @param pagination
         */
        setPagination(state, pagination) {
            const aroundItems = 2;
            const currentPage = pagination.current_page;
            const lastPage = pagination.last_page;
            const prevPage = currentPage - aroundItems > 1 ? currentPage - aroundItems : 1;
            const nextPage = currentPage + aroundItems <= lastPage ? currentPage + aroundItems : lastPage;
            const prevLink = pagination.links.shift();
            const nextLink = pagination.links.pop();
            const links = pagination.links.filter(link => {
                const page = 1 * link.label;
                return page >= prevPage && page <= nextPage;
            })
            pagination.links = [prevLink, ...links, nextLink];

            state.pagination = pagination;
        },

        /**
         * Sets the current page of the pagination
         * @param state
         * @param page
         */
        setCurrentPage(state, page) {
            state.page = page;
        },

        /**
         * Sets the loading status
         * @param state
         * @param loading
         */
        setLoading(state, loading) {
            state.loading = loading;
        },

        /**
         * Sets the loading status
         * @param state
         * @param type
         */
        setTypeOfLoading(state, type) {
            state.typeOfLoading = type;
        },
    },

    actions: {
        /**
         * Loads the Profile notifications
         */
        loadProfileNotifications({commit, state}) {
            commit('setLoading', true);
            axios.get('/notifications', {params: {page: state.page, type: state.typeOfLoading}})
                .then((response) => {
                    const items = response.data.data;
                    delete response.data.data;
                    const pagination = response.data;
                    const currentPage = response.data.current_page;

                    commit('setPaginateNotifications', items);
                    commit('setPagination', pagination);
                    commit('setCurrentPage', currentPage);
                    commit('setLoading', false);
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        markAsRead({commit, dispatch}, id = null) {
            commit('setLoading', true);

            let url = '/notifications/mark-read';
            if (id) {
                url += '/' + id;
            }

            axios.patch(url)
                .then((response) => {
                    if (response.status === 200) {
                        dispatch('loadProfileNotifications');
                    } else {
                        commit('setLoading', false);
                        console.log(response);
                    }
                })
                .catch((error) => {
                    commit('setLoading', false);
                    console.log(error);
                });
        },

        markAsReadAll({commit, dispatch}) {
            commit('setLoading', true);
            axios.patch('/notifications/mark-read-all')
                .then((response) => {
                    if (response.status === 200) {
                        dispatch('loadProfileNotifications');
                    } else {
                        commit('setLoading', false);
                    }
                })
                .catch((error) => {
                    commit('setLoading', false);
                    console.log(error);
                });
        },
    },
};

export {
    NOTIFICATION_TYPE_OF_VIGNETTE,
    TYPE_OF_LOAD_ALL,
    TYPE_OF_LOAD_NEW,
    notifications,
};
