/**
 * The storage of modals
 */
const modals = {
    namespaced: true,

    state: {
        registration: false,
        registerTab: 'private',
        login: false,
        forgotPassword: false,
        profileAddSign: false,
        profileRemoveSign: false,
    },

    getters: {
        /**
         * Returns visibility state of Registration modal
         * @param state
         */
        getRegisterVisibility(state) {
            return state.registration;
        },

        /**
         * Returns visibility state of Login modal
         * @param state
         */
        getLoginVisibility(state) {
            return state.login;
        },

        /**
         * Returns selected tab of register modal
         * @param state
         */
        getRegisterTab(state) {
            return state.registerTab;
        },

        /**
         * Returns visibility state of the Forgot Password modal
         * @param state
         */
        getForgotPasswordVisibility(state) {
            return state.forgotPassword;
        },

        /**
         * Returns visibility state of the Profile Add Sign modal
         * @param state
         */
        getProfileAddSignVisibility(state) {
            return state.profileAddSign;
        },

        /**
         * Returns visibility state of the Profile Remove Sign modal
         * @param state
         */
        getProfileRemoveSignVisibility(state) {
            return state.profileRemoveSign;
        },
    },

    mutations: {
        /**
         * Toggle visibility Registration modal
         * @param state
         */
        toggleRegistration(state) {
            state.registration = !state.registration;
        },

        /**
         * Sets selected register tab
         * @param state
         */
        setRegisterTab(state, { tab }) {
            state.registerTab = tab;
        },

        /**
         * Toggle visibility Login modal
         * @param state
         */
        toggleLogin(state) {
            state.login = !state.login;
        },

        /**
         * Toggle visibility the Forgot Password modal
         * @param state
         */
        toggleForgotPassword(state) {
            state.forgotPassword = !state.forgotPassword;
        },

        /**
         * Toggle visibility the Profile Add Sign modal
         * @param state
         */
        toggleProfileAddSign(state) {
            state.profileAddSign = !state.profileAddSign;
        },

        /**
         * Toggle visibility the Profile Remove Sign modal
         * @param state
         */
        toggleProfileRemoveSign(state) {
            state.profileRemoveSign = !state.profileRemoveSign;
        },
    },
};

export default modals;
