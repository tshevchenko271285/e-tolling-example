const CHOOSE_VIGNETTES_STORAGE_KEY = 'et-choose-vignettes'

export const Vignette = function (params = {}) {
    return {
        country: params.country ? params.country : 'UA',
        sign: params.sign ? params.sign : null,
        vignetteId: params.vignetteId ? params.vignetteId : null,
        start: params.start ? params.start : null,
        end: params.end ? params.end : null,
    };
};

/**
 * The storage of Choose Vignettes
 */
export const chooseVignettes = {
    namespaced: true,

    state: {
        vignettes: [],
        errors: {}
    },

    getters: {
        getVignettes(state) {
            return state.vignettes;
        },

        getErrors(state) {
            return state.errors;
        },

        hasErrors(state) {
            return !!Object.keys(state.errors).length;
        },
    },

    mutations: {
        setVignettes(state, { vignettes }) {
            state.vignettes = vignettes;
        },

        updateVignette(state, { vignette, index }) {
            state.vignettes = state.vignettes.map((item, key) => key === index ? vignette : item);
        },

        removeItem(state, { index }) {
            state.vignettes = state.vignettes.filter((item, key) => key !== index);
        },

        addEmptyVignette(state) {
            state.vignettes.push(new Vignette());
        },

        setErrors(state, { errors }) {
            state.errors = errors;
        }
    },

    actions: {
        /**
         * Loads the Vignettes from LocalStorage or create the empty Vignette
         * @param commit
         */
        loadState({ commit }) {
            let loadedData = localStorage.getItem(CHOOSE_VIGNETTES_STORAGE_KEY);
            localStorage.removeItem(CHOOSE_VIGNETTES_STORAGE_KEY);

            if(loadedData) {
                loadedData = JSON.parse(loadedData);
            } else {
                loadedData = [new Vignette()];
            }

            commit('setVignettes', { vignettes: loadedData });
        },

        /**
         * Saves Vignettes from the storage in the LocalStorage
         * @param state
         */
        saveState({ state }) {
            localStorage.setItem(CHOOSE_VIGNETTES_STORAGE_KEY, JSON.stringify(state.vignettes));
        },

        /**
         * Adds the vignettes to order
         * @param commit
         * @param vignettes
         */
        toOrder({ commit, state }) {
            commit('setErrors', { errors: {} });
            axios.post('/cart/add', { vignettes: state.vignettes })
                .then((response) => {
                    if (response.status === 200) {
                        window.location.replace('/cart');
                    }
                })
                .catch((error) => {
                    commit('setErrors', { errors: error.response.data.errors });
                });
        },

        /**
         * Validate the vignettes
         * @param commit
         * @param vignettes
         */
        validate({ commit, state }) {
            commit('setErrors', { errors: {} });
            axios.post('/vignettes/validate', { vignettes: state.vignettes })
                .then((response) => {
                    if (response.status === 202) {
                        commit('setErrors', { errors: {} });
                    }
                })
                .catch((error) => {
                    commit('setErrors', { errors: error.response.data.errors });
                });
        }
    },
};
