import Vignette from "../../../Models/MyVignete";

/**
 * This param for loading all vignettes
 * @type {string}
 */
const LOAD_TYPE_ALL = 'all';

/**
 * This param for loading only actual vignettes
 * @type {string}
 */
const LOAD_TYPE_ACTUAL = 'actual';

/**
 * The storage of my Vignettes
 */
const myVignettes = {
    namespaced: true,

    state: {
        vignettes: [],
        pagination: null,
        currentPage: 1,
        loadType: LOAD_TYPE_ACTUAL,
        search: '',
        loading: true,
    },

    getters: {
        /**
         * Returns vignettes from storage
         * @param state
         */
        getVignettes(state) {
            return state.vignettes;
        },

        /**
         * Returns the vignettes object pagination
         * @param state
         */
        getPagination(state) {
            return state.pagination;
        },

        /**
         * Returns the current page of pagination
         * @param state
         */
        getCurrentPage(state) {
            return state.currentPage;
        },

        /**
         * Returns the search query
         * @param state
         */
        getSearch(state) {
            return state.search;
        },

        /**
         * Returns type of loading vignettes
         * @param state
         */
        getLoadType(state) {
            return state.loadType;
        },

        /**
         * Returns the loading status
         * @param state
         */
        getLoading(state) {
            return state.loading;
        },
    },

    mutations: {
        /**
         * Saving vignettes in the storage
         * @param state
         * @param vignettes
         */
        setVignettes(state, vignettes) {
            state.vignettes = vignettes.map(item => new Vignette(item));
        },

        /**
         * Saving the vignettes pagination in the storage
         * @param state
         * @param roads
         */
        setPagination(state, pagination) {
            const aroundItems = 2;
            const currentPage = pagination.current_page;
            const lastPage = pagination.last_page;
            const prevPage = currentPage - aroundItems > 1 ? currentPage - aroundItems : 1;
            const nextPage = currentPage + aroundItems <= lastPage ? currentPage + aroundItems : lastPage;
            const prevLink = pagination.links.shift();
            const nextLink = pagination.links.pop();
            const links = pagination.links.filter(link => {
                const page = 1 * link.label;
                return page >= prevPage && page <= nextPage;
            })
            pagination.links = [prevLink, ...links, nextLink];

            state.pagination = pagination;
        },

        /**
         * Sets current page of the vignettes pagination
         * @param state
         * @param currentPage
         */
        setCurrentPage(state, currentPage) {
            state.currentPage = currentPage;
        },

        /**
         * Sets search data
         * @param state
         * @param currentPage
         */
        setSearch(state, search) {
            state.search = search.toString();
        },

        /**
         * Sets type of load vignettes
         * @param state
         * @param type
         */
        setLoadType(state, { type }) {
            state.loadType = type;
        },

        /**
         * Sets loading status
         * @param state
         * @param loading
         */
        setLoading(state, loading) {
            state.loading = loading;
        }
    },

    actions: {
        /**
         * Loads the vignettes of the user
         * @param commit
         * @param state
         */
        loadVignettes({ commit, state }) {
            const params = {
                type: state.loadType,
                search: state.search,
                page: state.currentPage,
            };

            commit('setLoading', true);

            axios.get('/profile/vignettes', { params })
                .then((response) => {
                    const vignettes = response.data.data;
                    const pagination = response.data.meta;
                    const currentPage = pagination.current_page;

                    commit('setVignettes', vignettes);
                    commit('setPagination', pagination);
                    commit('setCurrentPage', currentPage);
                    commit('setLoading', false);
                })
                .catch((error) => {
                    console.log(error);
                    commit('setLoading', false);
                });
        },
    },
};

export {
    myVignettes,
    LOAD_TYPE_ALL,
    LOAD_TYPE_ACTUAL,
};
