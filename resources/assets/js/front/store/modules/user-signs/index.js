/**
 * The storage of userSigns
 */
const userSigns = {
    namespaced: true,

    state: {
        signs: {},
        paginateSigns: {},
        page: 1,
        pagination: null,
        loading: false,
        search: '',
        errors: {},
    },

    getters: {
        /**
         * Returns the validation errors
         * @param state
         */
        getErrors(state) {
            return state.errors;
        },

        /**
         * Returns User signs from storage
         * @param state
         */
        getSigns(state) {
            return state.signs;
        },

        /**
         * Returns User signs from storage
         * @param state
         */
        getPaginateSigns(state) {
            return state.paginateSigns;
        },

        /**
         * Returns the pagination object for the user signs
         * @param state
         */
        getPagination(state) {
            return state.pagination;
        },

        /**
         * Returns the current page of the pagination
         * @param state
         */
        getCurrentPage(state) {
            return state.page;
        },

        /**
         * Returns the loading status
         * @param state
         */
        getLoading(state) {
            return state.loading;
        },

        /**
         * Returns the search query
         * @param state
         */
        getSearch(state) {
            return state.search;
        },
    },

    mutations: {
        /**
         * Savings the validation errors in the storage
         * @param state
         * @param errors
         */
        setErrors(state, errors) {
            state.errors = errors;
        },

        /**
         * Saving User signs in the storage
         * @param state
         * @param signs
         */
        setSigns(state, signs) {
            state.signs = signs;
        },

        /**
         * Saving User signs in the storage
         * @param state
         * @param paginateSigns
         */
        setPaginateSigns(state, paginateSigns) {
            state.paginateSigns = paginateSigns;
        },

        /**
         * Sets the pagination object of the signs
         * @param state
         * @param pagination
         */
        setPagination(state, pagination) {
            const aroundItems = 2;
            const currentPage = pagination.current_page;
            const lastPage = pagination.last_page;
            const prevPage = currentPage - aroundItems > 1 ? currentPage - aroundItems : 1;
            const nextPage = currentPage + aroundItems <= lastPage ? currentPage + aroundItems : lastPage;
            const prevLink = pagination.links.shift();
            const nextLink = pagination.links.pop();
            const links = pagination.links.filter(link => {
                const page = 1 * link.label;
                return page >= prevPage && page <= nextPage;
            })
            pagination.links = [prevLink, ...links, nextLink];

            state.pagination = pagination;
        },

        /**
         * Sets the current page of the pagination
         * @param state
         * @param page
         */
        setCurrentPage(state, page) {
            state.page = page;
        },

        /**
         * Sets the loading status
         * @param state
         * @param loading
         */
        setLoading(state, loading) {
            state.loading = loading;
        },

        /**
         * Sets search data
         * @param state
         * @param search
         */
        setSearch(state, search) {
            state.search = search.toString();
        },
    },

    actions: {
        /**
         * Loads the User signs
         */
        loadSigns({ commit }) {
            axios.get('/user/signs')
                .then((response) => {
                    commit('setSigns', response.data.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        /**
         * Loads the Profile signs
         */
        loadProfileSigns({ commit, state }) {
            commit('setLoading', true)
            axios.get('/profile/get-signs', { params: { page: state.page, search: state.search } })
                .then((response) => {
                    const paginateSigns = response.data.data;
                    const pagination = response.data.meta;
                    const currentPage = pagination.current_page;

                    commit('setPaginateSigns', paginateSigns);
                    commit('setPagination', pagination);
                    commit('setCurrentPage', currentPage);
                    commit('setLoading', false);
                })
                .catch((error) => {
                    console.log(error.response.data.errors);
                    commit('setLoading', false);
                });
        },

        /**
         * Sends a new sign for saving in DB
         */
        async add({ commit, dispatch }, sign) {
            let result = false;
            
            commit('setLoading', true);
            await axios.post('/profile/signs', { sign })
                .then((response) => {
                    if (response.status === 201) {
                        result = true;
                        dispatch('loadProfileSigns');
                    } else {
                        commit('setLoading', false)
                    }
                })
                .catch((error) => {
                    commit('setErrors', error.response.data.errors);
                    commit('setLoading', false)
                });

            return result;
        },

        /**
         * Removes the user sign
         *
         * @param commit
         * @param dispatch
         * @param sign
         * @returns {Promise<void>}
         */
        async remove({ commit, dispatch }, sign) {
            await axios.delete('/profile/signs/' + sign.id)
                .then((response) => {
                    if (response.status === 204) {
                        dispatch('loadProfileSigns');
                    }
                })
                .catch((error) => {
                    console.log(error.response);
                });
        }
    },
};

export default userSigns;
