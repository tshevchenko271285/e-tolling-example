/**
 * The storage of products
 */
const products = {
    namespaced: true,

    state: {
        products: [],
    },

    getters: {
        /**
         * Returns Products from storage
         * @param state
         */
        getProducts(state) {
            return state.products;
        },
    },

    mutations: {
        /**
         * Saving the Products in the storage
         * @param state
         * @param roads
         */
        setProducts(state, { products }) {
            state.products = products;
        },
    },

    actions: {
        /**
         * Loads the Products
         */
        loadProducts({ commit }) {
            axios.get('/products/list')
                .then((response) => {
                    commit('setProducts', { products: response.data.data });
                })
                .catch((error) => {
                    console.log(error);
                });
        },
    },
};

export default products;
