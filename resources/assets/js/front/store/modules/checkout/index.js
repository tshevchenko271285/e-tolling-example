/**
 * Model of the order
 * @param data
 * @constructor
 */
const Order = function(data = {}) {
    return {
        type_of_person: data.type_of_person ? data.type_of_person : 'private',
        name: data.name ? data.name : '',
        surname: data.surname ? data.surname : '',
        country: data.country ? data.country : '',
        postcode: data.postcode ? data.postcode : '',
        address: data.address ? data.address : '',
        city: data.city ? data.city : '',
        phone: data.phone ? data.phone : '',
        email: data.email ? data.email : '',
        company_name: data.company_name ? data.company_name : '',
        tax_number: data.tax_number ? data.tax_number : '',
        payment_type: data.payment_type ? data.payment_type : '',
        rules_confirmed: data.rules_confirmed ? data.rules_confirmed : false,
    }
}

/**
 * The storage of checkout
 */
const checkout = {
    namespaced: true,

    state: {
        order: new Order(),
        errors: {},
    },

    getters: {
        /**
         * Returns User errors from storage
         * @param state
         */
        getErrors(state) {
            return state.errors;
        },

        /**
         * Returns the order of the storage
         * @param state
         */
        getOrder(state) {
            return state.order;
        },
    },

    mutations: {
        /**
         * Saving User errors in the storage
         * @param state
         * @param roads
         */
        setErrors(state, { errors }) {
            state.errors = errors;
        },

        /**
         * Updates the order`s property
         * @param state
         * @param data
         */
        setOrderProps(state, data) {
            for (const key in data) {
                if(state.order.hasOwnProperty(key)) {
                    state.order[key] = data[key];
                }
            }
        },
    },

    actions: {
        async submitContacts({ commit }, { contacts }) {
            let result = false;
            await axios.put('/cart/contacts', contacts)
                .then((response) => {
                    result = true;
                })
                .catch((error) => {
                    commit('setErrors', { errors: error.response.data.errors });
                });

            return result;
        }
    },
};

export default checkout;
