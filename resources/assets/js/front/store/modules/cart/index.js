/**
 * The storage of cart
 */
const cart = {
    namespaced: true,

    state: {
        id: null,
        items: {},
        total: 0,
        googlePayFee: null,
        masterpassFee: null,
        loading: true,
        loaded: false,
        hasProducts: false,
        successOrder: null,
    },

    getters: {
        /**
         * Returns the order id of the current cart
         * @param state
         */
        getId(state) {
            return state.id;
        },

        /**
         * Returns the items of the Cart
         * @param state
         */
        getItems(state) {
            return state.items;
        },

        /**
         * Returns the fee for the Google Pay system.
         * @param state
         */
        getGooglePayFee(state) {
            return state.googlePayFee;
        },

        /**
         * Returns the fee for the Masterspass system.
         * @param state
         */
        getMasterpassFee(state) {
            return state.masterpassFee;
        },

        /**
         * Returns the total of the Cart
         * @param state
         */
        getTotal(state) {
            return state.total;
        },

        /**
         * Returns the status of loading of the cart
         * @param state
         */
        getLoading(state) {
            return state.loading;
        },

        /**
         * Returns true if cart has some products
         * @param state
         * @returns {boolean}
         */
        hasProducts(state) {
            return state.items.length || state.hasProducts;
        },

        /**
         * Returns true if the cart has been loaded
         * @param state
         * @returns {boolean}
         */
        getLoaded(state) {
            return state.loaded;
        },

        /**
         * Returns the success order
         * @param state
         */
        getSuccessOrder(state) {
            return state.successOrder;
        },
    },

    mutations: {
        /**
         * Saving the Cart Data
         */
        setCart(state, cart) {
            state.id = cart.id;
            state.items = cart.items;
            state.total = cart.total;

            if(cart.items.length === 0) {
                cart.hasProducts = false;
            }

            if(!state.loaded) {
                state.loaded = true;
            }
        },

        /**
         * Sets the total Google Pay calculation fee
         * @param state
         * @param value
         */
        setGooglePayFee(state, value) {
            state.googlePayFee = value;
        },

        /**
         * Sets the total masterpass calculation fee
         * @param state
         * @param value
         */
        setMasterpassFee(state, value) {
            state.masterpassFee = value;
        },

        /**
         * Changing the status of loading
         * @param state
         * @param value
         */
        setLoading(state, value) {
            state.loading = value;
        },

        /**
         * Sets value checking cart items
         * @param state
         * @param value
         */
        setHasProducts(state, value) {
            state.hasProducts = value;
        },

        /**
         * Sets the success order
         * @param state
         * @param order
         */
        setSuccessOrder(state, order) {
            state.successOrder = order;
        },
    },

    actions: {
        /**
         * Loads items of the Cart
         */
        loadCart({ commit }) {
            commit('setLoading', true);
            axios.get('/cart/data')
                .then((response) => {
                    const cart = response.data.data
                    commit('setCart', cart);
                    commit('setHasProducts', !!cart.items.length);
                    commit('setLoading', false);

                    if('order' in cart) {
                        commit('checkout/setOrderProps', cart.order, { root: true });
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        /**
         * Checks items of the Cart
         */
        checkCart({ commit }) {
            axios.get('/cart/has-items')
                .then((response) => {
                    commit('setHasProducts', response.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        /**
         * Removes item from the cart
         */
        removeItem({ commit, dispatch }, { id }) {
            commit('setLoading', true);
            axios.delete('/cart/remove/' + id)
                .then((response) => {
                    if (response.status === 204) {
                        commit('setLoading', false);
                        dispatch('loadCart');
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        /**
         * Loads the cart fee calculation
         * @param commit
         */
        loadCartFee({ commit }) {
            axios.get('/cart/calculation')
                .then((response) => {
                    if (response.data.hasOwnProperty('google_pay')) {
                        commit('setGooglePayFee', response.data.google_pay);
                    }
                    if (response.data.hasOwnProperty('masterpass')) {
                        commit('setMasterpassFee', response.data.masterpass);
                    }
                })
                .catch((error) => {
                    console.log(error);
                });
        },

        /**
         * Returns current success order
         */
        loadSuccessOrder({ commit }) {
            axios.get('/cart/success-order')
                .then((response) => {
                    commit('setSuccessOrder', response.data);
                })
                .catch((error) => {
                    console.log(error);
                });
        },
    },
};

export default cart;
