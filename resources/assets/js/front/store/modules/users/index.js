/**
 * The storage of users
 */
const users = {
    namespaced: true,

    state: {
        errors: {},
        loginErrors: {},
        registerErrors: {},
        updateErrors: {},
        current: null,
    },

    getters: {
        /**
         * Returns the errors from storage
         * @param state
         */
        getErrors(state) {
            return state.errors;
        },

        /**
         * Returns the login form errors from storage
         * @param state
         */
        getLoginErrors(state) {
            return state.loginErrors;
        },

        /**
         * Returns the register errors from storage
         * @param state
         */
        getRegisterErrors(state) {
            return state.registerErrors;
        },

        /**
         * Returns update User errors from storage
         * @param state
         */
        getUpdateErrors(state) {
            return state.updateErrors;
        },

        /**
         * Returns the current user
         * @param state
         * @returns {null}
         */
        getCurrent(state) {
            return state.current;
        }
    },

    mutations: {
        /**
         * Saving User errors in the storage
         * @param state
         * @param roads
         */
        setErrors(state, {errors}) {
            state.errors = errors;
        },

        /**
         * Saving login form errors in the storage
         * @param state
         * @param roads
         */
        setLoginErrors(state, {errors}) {
            state.loginErrors = errors;
        },

        /**
         * Saving register form errors in the storage
         * @param state
         * @param roads
         */
        setRegisterErrors(state, {errors}) {
            state.registerErrors = errors;
        },

        /**
         * Savings Update User errors in the storage
         * @param state
         * @param roads
         */
        setUpdateErrors(state, {errors}) {
            state.updateErrors = errors;
        },

        /**
         * Stores the current user in the storage
         * @param state
         * @param roads
         */
        setCurrent(state, { user }) {
            state.current = user;
        },
    },

    actions: {
        /**
         * Sends a request to register a new user
         */
        async register({ commit }, { user }) {
            let result = false;
            await axios.post('/register', user)
                .then((response) => {
                    if (response.status === 201) {
                        result = true;
                    }
                })
                .catch((error) => {
                    commit('setRegisterErrors', {errors: error.response.data.errors});
                });

            return result;
        },

        /**
         * Performs a user authentication request
         */
        async login({ commit }, { credentials }) {
            let result = false;
            await axios.post('/login', credentials)
                .then((response) => {
                    if (response.status === 204) {
                        result = true;
                    }
                })
                .catch((error) => {
                    commit('setLoginErrors', {errors: error.response.data.errors});
                });

            return result;
        },

        /**
         * Loads the current user
         * @param commit
         */
        loadCurrent({ commit }) {
            axios.get('/user/current')
                .then((response) => {
                    commit('setCurrent', { user: response.data });
                })
                .catch((error) => {
                    commit('setErrors', { errors: error.response.data.errors });
                });
        },

        /**
         * Updates the current user
         *
         * @param commit
         * @param user
         * @returns {Promise<boolean>}
         */
        async update({ commit }, { user }) {
            let result = false;
            commit('setUpdateErrors', { errors: {} });
            await axios.post('/profile', user)
                .then((response) => {
                    result = true;
                    console.log(response.data);
                })
                .catch((error) => {
                    commit('setUpdateErrors', { errors: error.response.data.errors });
                });

            return result;
        },
    },
};

export default users;
