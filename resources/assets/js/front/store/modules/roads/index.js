/**
 * The storage of roads
 */
const roads = {
    namespaced: true,

    state: {
        roads: []
    },

    getters: {
        /**
         * Returns roads from storage
         * @param state
         */
        getRoads(state) {
            return state.roads;
        },
    },

    mutations: {
        /**
         * Saving roads in the storage
         * @param state
         * @param roads
         */
        setRoads(state, { roads }) {
            state.roads = roads;
        },
    },

    actions: {
        /**
         * Loads an array of roads and saves to the storage
         * @param commit
         */
        loadRoads({ commit }) {
            axios.get('/roads')
            .then((response) => {
                const roads = response.data.data.map((item, index) => {
                    item['index'] = index;
                    item['visible'] = true;

                    return item;
                });

                commit('setRoads', { roads })
            })
            .catch((error) => {
                console.log(error);
            });
        },
    },
};

export default roads;
