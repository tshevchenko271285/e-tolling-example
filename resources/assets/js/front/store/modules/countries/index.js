/**
 * The storage of countries
 */
const countries = {
    namespaced: true,

    state: {
        countries: {},
        countriesArr: [],
    },

    getters: {
        /**
         * Returns countries from storage
         * @param state
         */
        getCountries(state) {
            return state.countries;
        },

        /**
         * Returns array of countries from the storage
         * @param state
         */
        getCountriesArr(state) {
            return state.countriesArr;
        },
    },

    mutations: {
        /**
         * Saving countries in the storage
         * @param state
         * @param roads
         */
        setCountries(state, { countries }) {
            state.countries = countries;
        },

        /**
         * Saving countries in the storage
         * @param state
         * @param roads
         */
        setCountriesArr(state, { countriesArr }) {
            state.countriesArr = countriesArr;
        },
    },

    actions: {
        /**
         * Loads a json of counties and saves to the storage
         * @param commit
         */
        loadCountries({commit, state}) {
            if (state.countriesArr.length) {
                return;
            }

            const locale = Vue.prototype.appLocale;
            const url = locale ? `/js/${locale}-countries.json` : '/js/countries.json';

            axios.get(url)
            .then((response) => {
                const countries = Object.keys(response.data).sort().reduce(
                    (obj, key) => {
                        obj[key] = response.data[key];
                        return obj;
                    },
                    {}
                );
                const countriesArr = [];

                for (const [value, label] of Object.entries(countries)) {
                    countriesArr.push({ value, label: `${value} - ${label}` });
                }

                commit('setCountries', { countries })
                commit('setCountriesArr', { countriesArr })
            })
            .catch((error) => {
                console.log(error);
            });
        },
    },
};

export default countries;
