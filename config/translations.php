<?php

return [
    /**
     * List of included language files
     */
    'files' => [
        '/front/global.php',
        '/front/pagemap.php',
        '/front/contactform.php',
        '/front/modal_register.php',
        '/front/modal_login.php',
        '/front/testsignform.php',
        '/front/pagecart.php',
        '/front/pagechoosevignette.php',
        '/front/pagecheckout.php',
        '/front/modal_forgot_password.php',
        '/front/reset_password.php',
        '/front/profile.php',
    ],
];
