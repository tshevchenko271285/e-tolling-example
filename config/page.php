<?php

use Itmaster\Page\Contracts\Field\CheckboxField;
use Itmaster\Page\Contracts\Field\EditorField;
use Itmaster\Page\Contracts\Field\FileField;
use Itmaster\Page\Contracts\Field\TextField;

return [
    'templates' => [
        'default' => [
            'title' => 'За замовчуванням',
            'path' => null,
        ],
        'exampleTemplate' => [
            'title' => 'Приклад шаблону',
            'path' => 'templates.example',
            'fields' => [
                new TextField('fieldTextExample'),
                new CheckboxField('fieldCheckboxExample'),
                new FileField('fieldFileExample'),
                new EditorField('fieldEditorExample'),
            ],
        ],
        'faq' => [
            'title' => 'Сторінка FAQ',
            'path' => 'pages.faq',
            'fields' => [
                new FileField('form_image'),
                new TextField('questions_form_title'),
            ],
        ],
        'contact' => [
            'title' => 'Сторінка Контактів',
            'path' => 'pages.contact',
            'fields' => [
                new TextField('email'),
                new TextField('phone'),
                new FileField('form_image'),
            ],
        ],
        'map' => [
            'title' => 'Сторінка Мапи платиних доріг',
            'path' => 'pages.map',
        ],
        'home' => [
            'title' => 'Головна сторінка',
            'path' => 'pages.home',
            'fields' => [
                new EditorField('about_text'),
                new FileField('about_image'),
                new EditorField('test_sign_text'),
                new FileField('test_sign_image'),
            ],
        ],
        'simple' => [
            'title' => 'Звичайна текстова сторінка',
            'path' => 'pages.simple',
            'fields' => [
                new EditorField('text'),
            ],
        ],
    ],
];
