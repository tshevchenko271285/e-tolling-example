<?php

return [
    'google' => [
        'api_url' => env('GOOGLE_IPAY_URL'),
        'callback_url' => env('GOOGLE_IPAY_CALLBACK_URL'),
        'login' => env('GOOGLE_IPAY_LOGIN'),
        'key' => env('GOOGLE_IPAY_KEY'),
    ],

    'masterpass' => [
        'api_url' => env('MASTERPASS_IPAY_URL'),
        'login' => env('MASTERPASS_IPAY_LOGIN'),
        'key' => env('MASTERPASS_IPAY_KEY'),
    ],
];
