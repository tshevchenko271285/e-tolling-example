# Platform Laravel #


Installation
============

## Setup virtual host(s)
You will need to make sure your server meets the following requirements:
* PHP >= 8.0.2
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* Ctype PHP Extension
* JSON PHP Extension

## Composer
Follow steps described in installation guide for [linux](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx)
or [Win](https://getcomposer.org/doc/00-intro.md#installation-windows) depending on your platform
> **NOTE:** Install composer globally


## Create new project via composer
```bash
composer create-project --stability=dev --repository="https://package.demo-itmaster.com" itmaster/platform [name-project]
```

## Config
Set config in .env file (root directory)


## Running Migrations
To run all of your outstanding migrations, execute the migrate Artisan command:
```php
php artisan migrate
```

## Running Seeders
```php
php artisan db:seed --class=InitSeeder
```

## Permission for folders
Set permissions for folders
```bash
sudo chmod -R 0777 storage/
sudo chmod -R 0777 bootstrap/cache/
```

## Setup cron jobs
Example
```bash
#!sh
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
#
```

## Running Mix
Run NPM scripts:

 + for usage broadcasting
```
npm install laravel-echo

npm install pusher-js
```
 + vue-multiselect
 ```
npm install vue-multiselect --save
```

 + dev environment
```npm run dev```

 + minify output
```npm run production```

## Add roles to database
For add roles to database you may run the following command
```
php artisan manager:install
```

## Add Page to menu in admin panel
For add Page to menu in admin panel you may run the following command
```
php artisan page:install
```

## Add Theme to menu in admin panel
For add Theme to menu in admin panel you may run the following command
```
php artisan theme:install
```

## Add Seo to menu in admin panel
For add Seo to menu in admin panel you may run the following command
```
php artisan seo:install
```

## Add Support to menu in admin panel
For add Support to menu in admin panel you may run the following command
```
php artisan support:install
```

## Running Role Seeders
products, categories, orders, orderItems
```
php artisan db:seed --class=ETollingRolesSeeder
```

## Publish all available assets
Publishes the scripts, styles, views, translations and other assets
```
php artisan vendor:publish
```

## Reset translations
Reset translations for the Front use the command
```
php artisan trans:reset
```

# Development

## GooglePay variables 
To use the IPAY Google Pay service, you must fill in the fields below
* GOOGLE_IPAY_URL="The url of IPAY GooglePay service"
* GOOGLE_IPAY_CALLBACK_URL="Callback Handler URLs"
* GOOGLE_IPAY_LOGIN=""
* GOOGLE_IPAY_KEY=""
* MIX_GOOGLE_GATEWAY=""
* MIX_GOOGLE_GATEWAY_MERCHANT_ID=""
* MIX_GOOGLE_MERCHANT_ID=""
* MIX_GOOGLE_MERCHANT_NAME=""
* MIX_GOOGLE_MERCHANT_ENVIRONMENT="TEST | PRODUCTION"

## Mastepass variables 
To use the IPAY Mastepass service, you must fill in the fields below
MASTERPASS_IPAY_URL="The url of Masterpass service"
MASTERPASS_IPAY_LOGIN=""
MIX_MASTERPASS_IPAY_LOGIN=""
MASTERPASS_IPAY_KEY=""

## Menu
Use widget in Blade
```php
@widget('menu', ['code' => 'code-from-table-menus'])
```


<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for helping fund on-going Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell):

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[British Software Development](https://www.britishsoftware.co)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
