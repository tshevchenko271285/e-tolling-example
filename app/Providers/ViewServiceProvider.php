<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['auth.passwords.reset'], function () {
            \View::share('hideLoginButtons', true);
        });

        view()->composer(['pages.*'], function ($view) {
            $viewName = explode('.', $view->getName());
            view()->share('viewName', end($viewName));
        });

        view()->composer(['pages.profile*'], function ($view) {
            $notificationService = app()->make('App\Services\Contracts\INotificationService');
            $profileService = app()->make('App\Services\Contracts\IProfileService');

            view()->share('notificationService', $notificationService);
            view()->share('hasVignettesReminder', $profileService->hasReminder());
            view()->share('hasNewNotifications', $notificationService->hasNewNotifications());
        });
    }
}
