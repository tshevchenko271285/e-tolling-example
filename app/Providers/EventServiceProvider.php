<?php

namespace App\Providers;

use App\Models\Order;
use App\Observers\OrderObserver;
use App\Observers\SupportObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Itmaster\Support\Models\Support;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\CartSold' => [
            'App\Listeners\AddUserSignsAfterPurchase',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Support::observe(SupportObserver::class);
        Order::observe(OrderObserver::class);
    }
}
