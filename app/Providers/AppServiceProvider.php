<?php

namespace App\Providers;

use App\Contracts\Menu\MenuContract;
use App\Contracts\Menu\MenuContractInterface;
use App\Contracts\Snippet\SnippetContract;
use App\Contracts\Snippet\SnippetContractInterface;
use App\Services\AuthGoogleService;
use App\Services\Captcha;
use App\Services\CartService;
use App\Services\CategoryService;
use App\Services\Contracts\IAuthService;
use App\Services\Contracts\ICartService;
use App\Services\Contracts\ICategoryService;
use App\Services\Contracts\IFaqService;
use App\Services\Contracts\IHistoryService;
use App\Services\Contracts\INotificationService;
use App\Services\Contracts\IProductService;
use App\Services\Contracts\IProfileService;
use App\Services\Contracts\IReminderService;
use App\Services\Contracts\IRoadService;
use App\Services\Contracts\ITranslateStringService;
use App\Services\Contracts\IUrlService;
use App\Services\Contracts\IUserSignService;
use App\Services\Contracts\IVignetteService;
use App\Services\Contracts\IVisitKeyService;
use App\Services\FaqService;
use App\Services\HistoryService;
use App\Services\NotificationService;
use App\Services\ProfileService;
use App\Services\ProductService;
use App\Services\ReminderService;
use App\Services\RoadService;
use App\Services\SettingService;
use App\Services\TranslateStringService;
use App\Services\TranslateUrlService;
use App\Services\UrlService;
use App\Services\UserSignService;
use App\Services\VignetteService;
use App\Services\VisitKeyService;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(255);

        if (!$this->app->runningInConsole()) {
            $this->bladeDirectives();
        }

        Paginator::useBootstrapFour();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->bind('setting', function () {
            return new SettingService();
        });

        $this->app->bind('captcha', function () {
            return new Captcha();
        });

        $this->app->bind('translateUrl', function () {
            return new TranslateUrlService();
        });

        $this->app->bind(MenuContractInterface::class, MenuContract::class);
        $this->app->bind(SnippetContractInterface::class, SnippetContract::class);
        $this->app->bind(ICategoryService::class, CategoryService::class);
        $this->app->bind(IVisitKeyService::class, VisitKeyService::class);
        $this->app->bind(IHistoryService::class, HistoryService::class);
        $this->app->bind(IProductService::class, ProductService::class);
        $this->app->bind(ICartService::class, CartService::class);
        $this->app->bind(IRoadService::class, RoadService::class);
        $this->app->bind(IFaqService::class, FaqService::class);
        $this->app->bind(ITranslateStringService::class, TranslateStringService::class);
        $this->app->bind(IAuthService::class, AuthGoogleService::class);
        $this->app->bind(IUserSignService::class, UserSignService::class);
        $this->app->bind(IProfileService::class, ProfileService::class);
        $this->app->bind(IVignetteService::class, VignetteService::class);
        $this->app->bind(IReminderService::class, ReminderService::class);
        $this->app->bind(INotificationService::class, NotificationService::class);
        $this->app->bind(IUrlService::class, UrlService::class);
    }

    /**
     * @return void
     */
    private function bladeDirectives()
    {
        $packages = event(config('platform.event_admin_dashboard_name'));
        $hasRoleModule = in_array(config('platform.role_module_name'), Arr::pluck($packages, 'module'));
        $hasPageModule = in_array(config('platform.page_module_name'), Arr::pluck($packages, 'module'));

        \View::share('packages', $packages);
        \View::share('hasRoleModule', $hasRoleModule);
        \View::share('hasPageModule', $hasPageModule);

        \Blade::directive('userCan', function ($expression) {
            return "<?php if (!\$hasRoleModule || (\$hasRoleModule && \\Permission::hasAnyPermission({$expression}))) : ?>";
        });
        \Blade::directive('endUserCan', function () {
            return "<?php endif; ?>";
        });
    }
}
