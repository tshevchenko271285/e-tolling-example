<?php

namespace App\Notifications;

use App\Models\Order;
use App\Services\Contracts\IUrlService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CartCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The service of the URL generation
     *
     * @var IUrlService
     */
    protected IUrlService $urlService;

    /**
     * Create a new notification instance.
     *
     * @return void
     * @throws BindingResolutionException
    */
    public function __construct(
        protected Order $order
    ) {
        $this->urlService = app()->make(IUrlService::class);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Це тестове повідомлення для перевірки лінка!')
                    ->action('Сплатити', $this->urlService->generateCartUrl($this->order->id))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
