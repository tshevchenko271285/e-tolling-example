<?php

namespace App\Notifications;

use App\Models\OrderItem;
use App\Models\Reminder;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class VignetteReminderNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        public OrderItem $orderItem,
        public Reminder  $reminder,
    )
    {
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->orderItem->order->user ? ['mail', 'database'] : ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from(env('ADMIN_MAIL'), env('APP_NAME'))
            ->line('Віньєтка для авто ' . $this->orderItem->car_sign)
            ->line(' закінчується ' . $this->orderItem->finished_at)
            ->action('Купити', url('/#prices'))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->orderItem->id,
            'product_id' => $this->orderItem->product->id,
            'sign' => $this->orderItem->car_sign,
            'country' => $this->orderItem->car_country,
            'finished_at' => $this->orderItem->finished_at,
            'term' => $this->reminder->value,
        ];
    }
}
