<?php

namespace App\Services;

use App\Services\Contracts\IVisitKeyService;
use Illuminate\Support\Str;

/**
 * The class generates a site visit key
 */
class VisitKeyService implements IVisitKeyService
{

    /**
     * Session key
     *
     * The name of the session key used
     * to store the unique visit key.
     * @var string
     */
    const SESSION_KEY = 'history_visit_key';

    /**
     * Visit key length
     *
     * The name of the session key used
     * to store the unique visit key.
     * @var string
     */
    const SESSION_KEY_LENGTH = 25;

    /**
     * Returns the visit key from the session
     * if not, a new key will be generated in the session
     * @return string
     */
    public function getVisitKey(): string
    {
        return session(self::SESSION_KEY) ?: self::generateVisitKey();
    }

    /**
     * Generates and saves a new visit key in the session
     * @return string
     */
    public function generateVisitKey(): string
    {
        $string = mb_substr(base64_encode(Str::orderedUuid()), 0, self::SESSION_KEY_LENGTH);
        session([self::SESSION_KEY => $string]);

        return $string;
    }
}
