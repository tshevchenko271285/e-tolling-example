<?php

namespace App\Services;

use App\Models\Reminder;
use App\Services\Contracts\IReminderService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

/**
 * The class contains business logic for Reminders
 */
class ReminderService implements IReminderService
{
    /**
     * Number of reminders on each page.
     */
    const DEFAULT_PAGINATION = 15;

    /**
     * Returns collection of the Reminders
     *
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator
    {
        return Reminder::paginate(self::DEFAULT_PAGINATION);
    }
}
