<?php

namespace App\Services;

use App\Models\OrderItem;
use App\Notifications\PasswordChangedNotification;
use App\Services\Contracts\IProfileService;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

/**
 * The class contains business logic for User Profile
 */
class ProfileService implements IProfileService
{
    /**
     * Count of vignettes on the page
     */
    const VIGNETTES_PER_PAGE = 6;

    /**
     * This param for loading all vignettes
     */
    const LOAD_TYPE_ALL = 'all';

    /**
     * This param for loading only actual vignettes
     */
    const LOAD_TYPE_ACTUAL = 'actual';

    /**
     * Updates data of the current user
     *
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool
    {
        $user = \Auth::user();

        if(!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
            $user->notify(new PasswordChangedNotification());
        }

        return $user->update($data);
    }

    /**
     * Returns the vignettes of the current user
     *
     * @param array $params
     * @return Paginator
     */
    public function getVignettes(array $params): Paginator
    {
        $paginator = OrderItem::with(['order', 'product', 'product.categories'])
            ->orderByDesc('id')
            ->whereRelation('order', 'user_id', \Auth::id())
            ->whereRelation('order', 'paid', '!=', null)
            ->when($params['type'] === self::LOAD_TYPE_ACTUAL, function ($query) {
                $query->where('finished_at', '>=', now()->startOfDay());
            })
            ->when(!empty($params['search']), function ($query) use ($params) {
                $query->where('car_sign', 'like', '%' . $params['search'] . '%');
            })
            ->paginate(self::VIGNETTES_PER_PAGE);

        $paginator->appends('type', $params['type']);
        $paginator->appends('search', $params['search']);

        return $paginator;
    }

    /**
     * Checks if the current user has any vignettes that are about to expire.
     * @return bool
     */
    public function hasReminder(): bool
    {
        return DB::table('order_items')
            ->join('products', 'order_items.product_id', '=', 'products.id')
            ->join('orders', 'order_items.order_id', '=', 'orders.id')
            ->select(['order_items.id'])
            ->whereNotNull('orders.paid')
            ->where('orders.user_id', \Auth::id())
            ->whereRaw('NOW() > DATE_SUB(order_items.finished_at, INTERVAL products.change_status DAY)')
            ->whereRaw('NOW() < order_items.finished_at')
            ->count() > 0;
    }
}
