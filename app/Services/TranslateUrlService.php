<?php

namespace App\Services;

use App\Services\Contracts\ITranslateUrlService;

/**
 * Helper class for translating URLs
 */
class TranslateUrlService implements ITranslateUrlService
{
    /**
     * Current URI without locale
     *
     * @var string
     */
    protected string $currentUri = '';

    /**
     * Removes the locale from the current URI
     * and stores it in the appropriate property.
     */
    public function __construct()
    {
        try {
            $this->currentUri = $this->removeLocale(request()->path());
        } catch (\Throwable $e) {
            report($e);
        }
    }

    /**
     * Removes the locale from the URI
     *
     * @param string $uri
     * @return string
     */
    protected function removeLocale(string $uri): string
    {
        $data = explode('/', $uri);
        if (
            !empty($data[0]) &&
            in_array($data[0], config('app.available_locales', []))
        ) {
            array_shift($data);
        }

        return implode('/', $data);
    }

    /**
     * Returns the current URI depending on the locale
     *
     * @param string $locale
     * @return string
     */
    public function current(string $locale): string
    {
        return '/' . $locale . ($this->currentUri ? '/' . $this->currentUri : '');
    }
}
