<?php

namespace App\Services;

use App\Models\Exceptions\ModelException;
use App\Models\Road;
use App\Models\RoadTranslate;
use App\Services\Contracts\IRoadService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;

/**
 * Contains business logic for working with the Road
 */
class RoadService implements IRoadService
{
    /**
     * Contains the coordinates of the parsed file
     *
     * @var array
     */
    protected array $coordinates = [];

    /**
     * Returns a collection of roads.
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Road::whereNotNull('coordinates')->get();
    }

    /**
     * Returns a paginated list of roads
     *
     * @param int $page
     * @return mixed
     */
    public function paginate(int $page = 25): mixed
    {
        return Road::paginate($page);
    }

    /**
     * Create the specified resource in storage.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data): void
    {
        $data = $this->prepareData($data);
        $translates = $data['translates'];
        $roadData = $data['road'];

        $road = Road::create($roadData);
        $road->translates()->createMany($translates);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Road $road
     * @param array $data
     * @return void
     */
    public function update(Road $road, array $data): void
    {
        $data = $this->prepareData($data);
        $translates = $data['translates'];
        $roadData = $data['road'];

        $road->update($roadData);
        DB::beginTransaction();
        try {
            foreach ($translates as $translate) {
                RoadTranslate::where('road_id', $road->id)
                    ->where('lang', $translate['lang'])
                    ->update($translate);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw new ModelException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Returns an array with data prepared for saving
     *
     * @param array $data
     * @return array
     */
    protected function prepareData(array $data): array
    {
        if (!empty($data['coordinates'])) {
            $data['coordinates'] = $this->parseCoordinates($data['coordinates']);
        }

        if (empty($data['coordinates'])) {
            unset($data['coordinates']);
        }

        if (!empty($data['title']) && is_array($data['title'])) {
            $translates = [];
            foreach ($data['title'] as $lang => $title) {
                $translates[] = [
                    'lang' => $lang,
                    'title' => $title,
                ];
            }
            unset($data['title']);

            $data = [
                'road' => $data,
                'translates' => $translates,
            ];
        }

        return $data;
    }

    /**
     * Parses the loaded XML and returns an array with route coordinates.
     *
     * @param UploadedFile $uploadedFile
     * @return array
     */
    protected function parseCoordinates(UploadedFile $uploadedFile): array
    {
        $this->coordinates = [];

        try {
            $doc = new \DOMDocument('1.0', 'utf-8');
            $doc->loadXML($uploadedFile->get());
            $lines = $doc->getElementsByTagName('LineString');
            foreach ($lines as $line) {
                $coordinates = $line->getElementsByTagName('coordinates');
                $this->parseLineCoordinates($coordinates->length ? $coordinates[0]->nodeValue : '');
            }
        } catch (\Throwable $exception) {
            report($exception);
        }

        $data = $this->coordinates;
        $this->coordinates = [];

        return $data;
    }

    /**
     * Parses and saves coordinates from a line
     *
     * @param string $str
     * @return void
     */
    protected function parseLineCoordinates(string $str): void
    {
        $rows = explode("\n", $str);
        foreach ($rows as $row) {
            $rowData = explode(',', $row);
            if (!empty($rowData[0]) && !empty($rowData[1])) {
                $this->coordinates[] = [
                    'lng' => (float) trim($rowData[0]),
                    'lat' => (float) trim($rowData[1]),
                ];
            }
        }
    }
}
