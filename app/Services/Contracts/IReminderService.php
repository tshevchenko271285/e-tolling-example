<?php

namespace App\Services\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * The class contains business logic for Reminders
 */
interface IReminderService
{
    /**
     * Returns collection of the Reminders
     *
     * @return LengthAwarePaginator
     */
    public function paginate(): LengthAwarePaginator;
}
