<?php

namespace App\Services\Contracts;

/**
 * Helper class for translating URLs
 */
interface ITranslateUrlService
{
    /**
     * Returns the current URI depending on the locale
     *
     * @param string $locale
     * @return string
     */
    public function current(string $locale): string;
}
