<?php

namespace App\Services\Contracts;

use App\Models\UserHistory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * The class contains the business logic for user stories.
 */
interface IHistoryService
{
    /**
     * Adds an entry to the user's history if $message
     * was specified $_POST and $_GET will be excluded
     *
     * @param string $message
     * @return void
     */
    public function add(string $message = ''): void;

    /**
     * Returns a collection of user stories grouped
     * by visit id and sorted by date.
     *
     * @return LengthAwarePaginator
     */
    public function getList(): LengthAwarePaginator;

    /**
     * Returns a collection of user actions by ID.
     *
     * @param string $historyId
     * @return LengthAwarePaginator
     */
    public function getHistoryItems(string $historyId): LengthAwarePaginator;

    /**
     * Returns a specific story by id
     *
     * @param int $id
     * @return UserHistory
     */
    public function getDetails(int $id): UserHistory;

    /**
     * Returns a collection of stories that match the search results.
     *
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function search(array $data): LengthAwarePaginator;
}
