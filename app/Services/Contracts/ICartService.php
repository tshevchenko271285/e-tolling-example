<?php

namespace App\Services\Contracts;

use App\Models\Order;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

/**
 * The class contains business logic for working with Cart.
 */
interface ICartService
{
    /**
     * Returns a cart from the session or creates a new one
     *
     * @return Order
     */
    public function getCart(): Order;

    /**
     * Adds vignettes in the Cart
     *
     * @param array $data
     * @return Collection
     */
    public function addItems(array $data): Collection;

    /**
     * Removes an item from the cart
     *
     * @param int $id
     * @return bool
     */
    public function removeItem(int $id): bool;

    /**
     * Checks is has items in the cart
     *
     * @return bool
     */
    public function hasItems(): bool;

    /**
     * Sets the contact data of the cart order
     *
     * @param array $data
     * @return bool
     */
    public function setContacts(array $data): bool;

    /**
     * Sets the cart order as sold
     *
     * @param int $id
     * @return bool
     */
    public function toSellCart(int $id): bool;

    /**
     * Checks if there is a successful order in the session
     *
     * @return bool
     */
    public function hasSuccessOrder(): bool;

    /**
     * Returns current success order
     *
     * @return Order|null
     */
    public function getSuccessOrder(): ?Order;

    /**
     * Creates a cart for the manager
     *
     * @param array $data
     * @return void
     */
    public function createOrder(array $data): void;

    /**
     * Sets the Cart from the Request
     *
     * @param Request $request
     * @return void
     * @throws BindingResolutionException
     */
    public function setCart(Request $request): void;
}
