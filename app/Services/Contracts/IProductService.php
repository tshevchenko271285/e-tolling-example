<?php

namespace App\Services\Contracts;

use App\Models\Product;
use Illuminate\Database\Eloquent\Collection;

/**
 * The class contains business logic for working with products.
 */
interface IProductService
{
    /**
     * Returns a list of products ordered by category
     *
     * @return Collection
     */
    public function getByCategories(): Collection;

    /**
     * Returns a list of products ordered by category
     *
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * Creates the Product
     *
     * @param array $data
     * @return Product
     */
    public function create(array $data): Product;

    /**
     * Updates the product
     *
     * @param Product $product
     * @param array $data
     * @return bool
     */
    public function update(Product $product, array $data): bool;
}
