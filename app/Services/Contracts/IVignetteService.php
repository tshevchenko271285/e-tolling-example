<?php

namespace App\Services\Contracts;

use App\Models\OrderItem;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * The class contains business logic for Vignettes
 */
interface IVignetteService
{
    /**
     * Checks the actual vignette
     *
     * @param array $data
     * @return OrderItem|null
     */
    public function check(string $sign, string $country): ?OrderItem;

    /**
     * Returns a vignette pagination object
     *
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data = []): LengthAwarePaginator;

    /**
     * Search for previously purchased vignettes.
     *
     * @param array $data
     * @return LengthAwarePaginator.
     */
    public function search(array $data): LengthAwarePaginator;
}
