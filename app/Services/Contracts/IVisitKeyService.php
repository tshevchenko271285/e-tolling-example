<?php

namespace App\Services\Contracts;


/**
 * The class generates a site visit key
 */
interface IVisitKeyService
{
    /**
     * Returns the visit key from the session
     * if not, a new key will be generated in the session
     * @return string
     */
    public function getVisitKey(): string;

    /**
     * Generates and saves a new visit key in the session
     * @return string
     */
    public function generateVisitKey(): string;
}
