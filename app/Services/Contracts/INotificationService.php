<?php

namespace App\Services\Contracts;

use Illuminate\Contracts\Pagination\Paginator;

/**
 * The class contains business logic for User notifications
 */
interface INotificationService
{
    /**
     * Returns the current user's notifications
     * @param array $data
     * @return mixed
     */
    public function getNotifications(array $data): Paginator;

    /**
     * Marks notifications as read
     *
     * @param string|null $id
     * @return void
     */
    public function markAsRead(?string $id): void;

    /**
     * Checks has the user unread notifications
     *
     * @return bool
     */
    public function hasNewNotifications(): bool;

    /**
     * Sends user notifications about ending vignettes.
     *
     * @return void
     */
    public function sendVignetteNotifications(): void;

    /**
     * Removes old notifications
     *
     * @return void
     */
    public function removeOld(): void;
}
