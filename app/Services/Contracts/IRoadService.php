<?php

namespace App\Services\Contracts;

use App\Models\Road;
use Illuminate\Database\Eloquent\Collection;

/**
 * Contains business logic for working with the Road
 */
interface IRoadService
{
    /**
     * Returns a collection of roads.
     *
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * Returns a paginated list of roads
     *
     * @param int $page
     * @return mixed
     */
    public function paginate(): mixed;

    /**
     * Create the specified resource in storage.
     *
     * @param array $data
     * @return void
     */
    public function create(array $data): void;

    /**
     * Update the specified resource in storage.
     *
     * @param Road $road
     * @param array $data
     * @return void
     */
    public function update(Road $road, array $data): void;
}
