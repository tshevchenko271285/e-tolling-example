<?php

namespace App\Services\Contracts;

use Illuminate\Http\Request;

interface IUrlService
{
    /**
     * Generates a link with hashed the Cart ID
     *
     * @param int $id
     * @return string
     */
    public function generateCartUrl(int $id): string;

    /**
     * Returns the Cart ID from url
     *
     * @return string
     */
    public function parseCartUrl(Request $request): int;
}
