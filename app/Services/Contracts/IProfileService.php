<?php

namespace App\Services\Contracts;


use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * The class contains business logic for User Profile
 */
interface IProfileService
{
    /**
     * Updates data of the current user
     *
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool;

    /**
     * Returns the vignettes of the current user
     *
     * @param array $params
     * @return Paginator
     */
    public function getVignettes(array $params): Paginator;

    /**
     * Checks if the current user has any vignettes that are about to expire.
     * @return bool
     */
    public function hasReminder(): bool;
}
