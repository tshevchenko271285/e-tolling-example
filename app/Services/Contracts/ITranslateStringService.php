<?php

namespace App\Services\Contracts;

/**
 * Helper class for translating Strings
 */
interface ITranslateStringService
{
    /**
     * Returns the string to output in a JS file
     *
     * @param string $lang
     * @return string
     */
    public function getJS(string $lang): string;
}
