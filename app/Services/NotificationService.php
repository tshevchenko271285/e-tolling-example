<?php

namespace App\Services;

use App\Notifications\VignetteReminderNotification;
use App\Services\Contracts\INotificationService;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Support\Facades\Notification;

/**
 * The class contains business logic for User notifications
 */
class NotificationService implements INotificationService
{
    /**
     * Count of notifications on the page
     */
    const NOTIFICATIONS_PER_PAGE = 6;

    /**
     * Loading type - all.
     */
    const TYPE_OF_LOAD_ALL = 'all';

    /**
     * Loading type - new.
     */
    const TYPE_OF_LOAD_NEW = 'new';

    /**
     * Returns the current user's notifications
     * @param array $data
     * @return mixed
     */
    public function getNotifications(array $data): Paginator
    {
        if ($data['type'] === self::TYPE_OF_LOAD_NEW) {
            $query = \Auth::user()->unreadNotifications();
        } else {
            $query = \Auth::user()->notifications();
        }

        return $query->paginate(self::NOTIFICATIONS_PER_PAGE);
    }

    /**
     * Marks notifications as read
     *
     * @param string|null $id
     * @return void
     */
    public function markAsRead(?string $id): void
    {
        $user = \Auth::user();
        if ($id) {
            $user->unreadNotifications()->find($id)->markAsRead();
        } else {
            $user->unreadNotifications()->update(['read_at' => now()]);
        }
    }

    /**
     * Checks has the user unread notifications
     *
     * @return bool
     */
    public function hasNewNotifications(): bool
    {
        return (bool)\Auth::user()->unreadNotifications()->count();
    }

    /**
     * Sends user notifications about ending vignettes.
     *
     * @return void
     */
    public function sendVignetteNotifications(): void
    {
        $now = now()->endOfDay();
        $orderItems = \App\Models\OrderItem::with(['product', 'order', 'order.user', 'product.reminders'])
            ->where('finished_at', '>=', now()->startOfDay())
            ->whereRelation('order', 'paid', '!=', null)
            ->get();

        $orderItems->each(function ($item) use ($now) {
            $user = $item->order->user;
            foreach ($item->product->reminders as $reminder) {
                $term = $reminder->value;
                $reminderTime = $item->finished_at->sub($term, 'days')->endOfDay();
                if ($now->equalTo($reminderTime)) {
                    if (empty($user)) {
                        $name = $item->order->name . ' ' . $item->order->surname;
                        $email = $item->order->email;
                        $notifiable = Notification::route('mail', [$email => $name]);
                    } else {
                        $notifiable = $user;
                    }

                    $notifiable->notify(new VignetteReminderNotification($item, $reminder));
                }
            };
        });
    }

    /**
     * Removes old notifications
     *
     * @return void
     */
    public function removeOld(): void
    {
        DatabaseNotification::where('read_at', '<=', now()->endOfDay()->subMonths(3))->delete();
    }
}
