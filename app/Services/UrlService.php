<?php

namespace App\Services;

use App\Services\Contracts\IUrlService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;

class UrlService implements IUrlService
{
    /**
     * Time of live the cart url
     */
    const TIME_OF_LIFE_CART_URL = 7;

    /**
     * Generates a link with hashed the Cart ID
     *
     * @param int $id
     * @return string
     */
    public function generateCartUrl(int $id): string
    {
        return URL::temporarySignedRoute(
            'cart.set',
            now()->addDays(self::TIME_OF_LIFE_CART_URL),
            ['id' => urlencode(Crypt::encryptString($id))]
        );
    }

    /**
     * Returns the Cart ID from request
     *
     * @return string
     */
    public function parseCartUrl(Request $request): int
    {
        return (int) urldecode(Crypt::decryptString($request->id));
    }

    /**
     * Makes link for sorting request
     *
     * @param string $url
     * @param string $field
     * @return string
     */
    public static function getSortingUrl(string $url, string $field): string
    {
        $queryString = http_build_query(request()->except(['sort', 'order']));
        $url = $url . '?sort=' . $field;
        $url .=  '&order=' . ((request()->sort === $field && request()->order === 'DESC') ? 'ASC' : 'DESC');
        $url .= strlen($queryString) ? '&' . $queryString : '';

        return $url;
    }
}
