<?php

namespace App\Services;

use App\Events\CartSold;
use App\Models\Order;
use App\Models\Product;
use App\Services\Contracts\ICartService;
use App\Services\Contracts\IUrlService;
use Carbon\Carbon;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartService implements ICartService
{
    /**
     * Session key to store shopping cart ID
     */
    const CART_SESSION_KEY = 'e_tolling_cart';

    /**
     * Session key to store last success order
     */
    const SUCCESS_ORDER_SESSION_KEY = 'e_tolling_success_order';

    /**
     * Session key to store step of checkout page
     */
    const CHECKOUT_STEP_SESSION_KEY = 'e_tolling_checkout_step';

    /**
     * Returns a cart from the session or creates a new one
     *
     * @return Order
     */
    public function getCart(): Order
    {
        $orderId = session(self::CART_SESSION_KEY);

        $order = Order::firstOrCreate([
            'id' => $orderId,
            'paid' => null,
        ]);

        session([self::CART_SESSION_KEY => $order->id]);

        return $order;
    }

    /**
     * Adds vignettes in the Cart
     *
     * @param array $data
     * @return Collection
     */
    public function addItems(array $data): Collection
    {
        return $this->getCart()->items()->createMany($this->prepareItemsData($data));
    }

    /**
     * Removes an item from the cart
     *
     * @param int $id
     * @return bool
     */
    public function removeItem(int $id): bool
    {
        return (bool) $this->getCart()->items()->find($id)?->delete();
    }

    /**
     * Checks is has items in the cart
     *
     * @return bool
     */
    public function hasItems(): bool
    {
        $result = false;
        $orderId = session(self::CART_SESSION_KEY);

        if (!empty($orderId)) {
            $order = Order::select('id')
                ->where('id', $orderId)
                ->whereNull('paid')
                ->has('items')
                ->first();

            $result = (bool) $order;
        }

        return $result;
    }

    /**
     * Sets the contact data of the cart order
     *
     * @param array $data
     * @return bool
     */
    public function setContacts(array $data): bool
    {
        if (Auth::check()) {
            $data['user_id'] = Auth::id();
        }

        return $this->getCart()->update($data);
    }

    /**
     * Sets the cart order as sold
     *
     * @param int $id
     * @return bool
     */
    public function toSellCart(int $id): bool
    {
        $cart = Order::whereNull('paid')->where('id', $id)->firstOrFail();

        session([self::SUCCESS_ORDER_SESSION_KEY => $cart->id]);

        $result = $cart->update(['paid' => 1]);

        CartSold::dispatch($cart);

        return $result;
    }

    /**
     * Checks if there is a successful order in the session
     *
     * @return bool
     */
    public function hasSuccessOrder(): bool
    {
        return session()->has(self::SUCCESS_ORDER_SESSION_KEY);
    }

    /**
     * Returns current success order
     *
     * @return Order|null
     */
    public function getSuccessOrder(): ?Order
    {
        $result = null;
        $orderId = session(self::SUCCESS_ORDER_SESSION_KEY);

        if ($orderId) {
            session()->remove(self::SUCCESS_ORDER_SESSION_KEY);
            $result = Order::find($orderId);
        }

        return $result;
    }

    /**
     * Creates a cart for the manager
     *
     * @param array $data
     * @return void
     */
    public function createOrder(array $data): void
    {
        $vignettesData = $this->prepareItemsData($data['vignettes']);
        unset($data['vignettes']);
        $data['paid'] = null;

        $order = Auth::user()->sales()->create($data);
        $order->items()->createMany($vignettesData);
    }

    /**
     * Sets the Cart from the Request
     *
     * @param Request $request
     * @return void
     * @throws BindingResolutionException
     */
    public function setCart(Request $request): void
    {
        $cartID = app()->make(IUrlService::class)->parseCartUrl($request);
        $order = Order::where('id', $cartID)->whereNull('paid')->first();

        if($order) {
            session([self::CART_SESSION_KEY => $cartID]);
            session([self::CHECKOUT_STEP_SESSION_KEY => 2]);
        }
    }

    /**
     * Preparing data for creating order items
     *
     * @param array $data
     * @return array
     */
    protected function prepareItemsData(array $data): array
    {
        $data = collect($data);
        $productIds = $data->pluck('vignetteId');
        $products = Product::find($productIds);

        return $data->map(function ($item) use ($products) {
            $product = $products->first(fn($value) => $value->id == $item['vignetteId']);
            if ($product) {
                $startDate = Carbon::parse($item['start']);
                $startedAt = $startDate->startOfDay()->toDateTimeString();
                $finishedAt = $startDate->addDays($product->term - 1)->endOfDay()->toDateTimeString();

                return [
                    'product_id' => $product->id,
                    'car_country' => $item['country'],
                    'car_sign' => $item['sign'],
                    'started_at' => $startedAt,
                    'finished_at' => $finishedAt,
                    'product_data' => $product->toJson(),
                ];
            }
        })->filter(fn($item) => $item)->toArray();
    }
}
