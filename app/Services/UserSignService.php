<?php

namespace App\Services;

use App\Models\Order;
use App\Models\UserSign;
use App\Services\Contracts\IUserSignService;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

/**
 * The class contains business logic for User Signs
 */
class UserSignService implements IUserSignService
{
    /**
     * Number of signs per page
     */
    const PER_PAGE = 12;

    /**
     * Returns a list of signs of current user
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return Auth::check() ? Auth::user()->signs()->orderByDesc('id')->get() : collect();
    }

    /**
     * Returns a pagination of signs of current user
     *
     * @param array $params
     * @return Paginator
     */
    public function pagination(array $params): Paginator
    {
        return Auth::user()
            ->signs()
            ->orderByDesc('id')
            ->when(!empty($params['search']), function ($query) use ($params) {
                $query->where('sign', 'like', '%' . $params['search'] . '%');
            })
            ->paginate(self::PER_PAGE);
    }

    /**
     * Adds a new user sign
     *
     * @param array $data
     * @return UserSign
     */
    public function store(array $data): UserSign
    {
        return Auth::user()->signs()->create($data);
    }

    /**
     * Removes the sign by id
     *
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        return UserSign::find($id)->delete();
    }

    /**
     * Creates User Signs from the Order
     *
     * @param Order $order
     * @return void
     */
    public function createFromOrder(Order $order): void
    {
        if ($order->user) {
            $orderSigns = $order->items()
                ->select('car_sign')
                ->get()
                ->pluck('car_sign')
                ->unique()
                ->map(fn($item) => strtoupper($item));

            $existSigns = UserSign::whereIn('sign', $orderSigns)
                ->select('sign')
                ->get()
                ->pluck('sign')
                ->map(fn($item) => strtoupper($item));

            $notExistsSigns = $orderSigns
                ->diff($existSigns)
                ->unique()
                ->map(fn($item) => ['sign' => $item])
                ->all();

            $order->user->signs()->createMany($notExistsSigns);
        }
    }
}
