<?php

namespace App\Services;

use App\Models\UserHistory;
use App\Services\Contracts\IHistoryService;
use App\Services\Contracts\IVisitKeyService;
use Fuko\Masked\Protect;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * The class contains the business logic for user stories.
use Illuminate\Support\Facades\Auth;

/**
 * Class for adding data to the user's story
 */
class HistoryService implements IHistoryService
{
    /**
     * Used when automatically adding data
     *
     * @var string
     */
    const EVENT_TYPE_AUTO = 'auto';

    /**
     * Used when manually adding data
     *
     * @var string
     */
    const EVENT_TYPE_MANUAL = 'manual';

    /**
     * Number of records displayed on one page
     *
     * @var int
     */
    const PER_PAGE = 25;

    /**
     * Search form date format
     *
     * @var int
     */
    const SEARCH_DATE_FORMAT = 'MM/DD/YYYY';

    /**
     * Contains a custom message
     *
     * @var string
     */
    protected string $message;

    /**
     * Inject visit key service
     *
     * @param IVisitKeyService $visitKeyService
     */
    public function __construct(
        protected IVisitKeyService $visitKeyService
    )
    {
        Protect::hideInput('password', INPUT_POST);
    }

    /**
     * Adds an entry to the user's history if $message
     * was specified $_POST and $_GET will be excluded
     *
     * @param string $message
     * @return void
     */
    public function add(string $message = ''): void
    {
        $this->message = $message;
        $this->save($this->prepareData());
    }

    /**
     * Prepares a dataset for saving to a database
     *
     * @return array
     */
    protected function prepareData(): array
    {
        $data = [
            'visit_id' => $this->visitKeyService->getVisitKey(),
            'page' => !empty($_SERVER['REQUEST_URI']) ? strtok($_SERVER['REQUEST_URI'], '?') : '',
            'referer' => !empty($_SERVER['HTTP_REFERER']) ? strtok($_SERVER['HTTP_REFERER'], '?') : '',
            'parent_id' => '',
            'ip' => $_SERVER['REMOTE_ADDR'] ?? $_SERVER['HTTP_X_FORWARDED_FOR'] ?? '',
            'auth_type' => Auth::check() ? 'user' : '',
            'user_id' => Auth::id(),
        ];

        if (strlen($this->message) > 0) {
            $data['event_type'] = self::EVENT_TYPE_MANUAL;
            $data['message'] = $this->message;
        } else {
            $data['get'] = count($_GET) ? json_encode($_GET) : null;
            $data['post'] = count($_POST) ? json_encode(Protect::protect($_POST)) : null;
            $data['event_type'] = self::EVENT_TYPE_AUTO;
        }

        return $data;
    }

    /**
     * Stores data in the database
     *
     * @param array $data
     * @return void
     */
    protected function save(array $data): void
    {
        UserHistory::create($data);
    }

    /**
     * Returns a collection of user stories grouped
     * by visit id and sorted by date.
     *
     * @return LengthAwarePaginator
     */
    public function getList(): LengthAwarePaginator
    {
        return DB::table('user_histories')
            ->orderBy('created_at', 'DESC')
            ->select('visit_id', DB::raw('MAX(created_at) as created_at'))
            ->groupBy('visit_id')
            ->paginate(self::PER_PAGE);
    }

    /**
     * Returns a collection of user actions by ID.
     *
     * @param string $historyId
     * @return LengthAwarePaginator
     */
    public function getHistoryItems(string $historyId): LengthAwarePaginator
    {
        return UserHistory::where('visit_id', $historyId)
            ->with('user')
            ->orderBy('id', 'DESC')
            ->paginate(self::PER_PAGE);
    }

    /**
     * Returns a specific story by id
     *
     * @param int $id
     * @return UserHistory
     */
    public function getDetails(int $id): UserHistory
    {
        return UserHistory::find($id);
    }

    /**
     * Returns a collection of stories that match the search results.
     *
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function search(array $data): LengthAwarePaginator
    {
        $query = UserHistory::with('user');
        if (!empty($data['ip'])) {
            $query->where('ip', $data['ip']);
        }

        if (!empty($data['start_date']) && !empty($data['end_date'])) {
            $data['start_date'] = Carbon::parse($data['start_date'])->startOfDay();
            $data['end_date'] = Carbon::parse($data['end_date'])->endOfDay();
            $query->whereBetween('created_at', [$data['start_date'], $data['end_date']]);
        }

        return $query->orderBy('id', 'DESC')->paginate(self::PER_PAGE);
    }
}
