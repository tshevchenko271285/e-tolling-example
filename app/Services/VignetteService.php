<?php

namespace App\Services;

use App\Models\OrderItem;
use App\Services\Contracts\IVignetteService;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

/**
 * The class contains business logic for Vignettes
 */
class VignetteService implements IVignetteService
{
    /**
     * The number of elements to paginate the API response.
     */
    const API_DEFAULT_PER_PAGE = 15;

    /**
     * The number of elements to paginate on the Admin panel.
     */
    const ADMIN_PER_PAGE = 15;

    /**
     * Checks the actual vignette
     *
     * @param string $sign
     * @param string $country
     * @return ?OrderItem
     */
    public function check(string $sign, string $country): ?OrderItem
    {
        $now = now();

        return OrderItem::whereRelation('order', 'paid', '!=', null)
            ->where('car_sign', $sign)
            ->where('car_country', $country)
            ->where('finished_at', '>', $now)
            ->where('started_at', '<=', $now)
            ->orderByDesc('id')
            ->first(['id', 'started_at', 'finished_at']);
    }

    /**
     * Returns a vignette pagination object
     *
     * @param array $data
     * @return LengthAwarePaginator
     */
    public function get(array $data = []): LengthAwarePaginator
    {
        $now = now();
        $perPage =  $data['per_page'] ?? self::API_DEFAULT_PER_PAGE;

        $subQuery = DB::table('order_items')
            ->select('car_sign', 'car_country', DB::raw('MAX(started_at) as started_at'))
            ->where('started_at', '<=', $now)
            ->groupBy('car_sign', 'car_country');

        return DB::table('order_items')
            ->select(
                'order_items.car_country',
                'order_items.car_sign',
                'order_items.started_at',
                'order_items.finished_at'
            )
            ->joinSub($subQuery, 'sub_orders', function ($join) use ($now) {
                $join->on('order_items.car_sign', '=', 'sub_orders.car_sign')
                    ->on('order_items.car_country', '=', 'sub_orders.car_country')
                    ->on('order_items.started_at', '=', 'sub_orders.started_at');
            })
            ->when(
                !empty($data['country']),
                fn($query) => $query->where('order_items.car_country', 'like', '%' . $data['country'] . '%')
            )
            ->when(
                !empty($data['sign']),
                fn($query) => $query->where('order_items.car_sign', 'like', '%' . $data['sign'] . '%')
            )
            ->orderByDesc('order_items.started_at')
            ->distinct()
            ->paginate($perPage);
    }

    /**
     * Search for previously purchased vignettes.
     *
     * @param array $data
     * @return LengthAwarePaginator.
     */
    public function search(array $data): LengthAwarePaginator
    {
        $sort = $data['sort'] ?? 'id';
        $order = $data['order'] ?? 'DESC';

        return OrderItem::with(['order'])
            ->when(empty($data['manager']), function ($query) {
                $query->whereRelation('order', 'paid', '!=', null);
            })
            ->when(!empty($data['manager']), function ($query) {
                $query->whereRelation('order', 'manager_id', '!=', null);
            })
            ->when(!empty($data['search']), function ($query) use ($data) {
                $query->where('car_sign', 'like', '%' . $data['search'] . '%')
                    ->orWhereRelation('order', 'email', 'like', '%' . $data['search'] . '%');
            })
            ->when(!empty($data['date']), function ($query) use ($data) {
                $query->where('started_at', Carbon::parse($data['date']));
            })
            ->when(!empty($data['term']), function ($query) use ($data) {
                $query->where('product_data->term', $data['term']);
            })
            ->orderBy($sort, $order)
            ->paginate(self::ADMIN_PER_PAGE)->withQueryString();
    }
}
