<?php

namespace App\Console;

use App\Console\Commands\ClearOrders;
use App\Console\Commands\VignetteReminders;
use App\Services\SlugService;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            SlugService::saveSlugs();
        })->everyMinute();

        $schedule->command(VignetteReminders::class, ['--clear'])->dailyAt('12:00');

        $schedule->command(ClearOrders::class)->dailyAt('23:59');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
