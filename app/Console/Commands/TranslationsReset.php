<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class TranslationsReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trans:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush cache for JS translations. Should be called after every change to language files';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $languages = Config::get('app.available_locales', [Config::get('app.fallback_locale', 'en')]);
        foreach ($languages as $language) {
            $key = 'lang_'.$language.'.js';
            Cache::forget($key);
            $this->info('The ' . $language . ' translation cache has been removed');
        }
    }
}
