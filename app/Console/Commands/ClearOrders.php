<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;

class ClearOrders extends Command
{
    /**
     * Count of days before deletion
     */
    const DAYS_BEFORE_DELETION = 7;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears unpurchased orders';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $removedDate = now()->subDays(self::DAYS_BEFORE_DELETION)->startOfDay();
        Order::whereNull('paid')->where('created_at', '<=', $removedDate)->forceDelete();

        return Command::SUCCESS;
    }
}
