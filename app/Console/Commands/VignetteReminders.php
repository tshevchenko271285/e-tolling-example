<?php

namespace App\Console\Commands;

use App\Services\Contracts\INotificationService;
use Illuminate\Console\Command;

class VignetteReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vignettes:reminder {--clear}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends notifications to clients when a vignette expires.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(INotificationService $notificationService): int
    {
        if ($this->option('clear')) {
            $notificationService->removeOld();
        }

        $notificationService->sendVignetteNotifications();

        return Command::SUCCESS;
    }
}
