<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class TranslateUrl
 *
 * @package App\Facades
 */
class TranslateUrl extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'translateUrl';
    }
}
