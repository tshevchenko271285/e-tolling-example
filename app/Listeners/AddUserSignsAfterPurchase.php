<?php

namespace App\Listeners;

use App\Events\CartSold;
use App\Services\Contracts\IUserSignService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddUserSignsAfterPurchase implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        protected IUserSignService $userSignService
    ) {}

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CartSold $event)
    {
        $this->userSignService->createFromOrder($event->order);
    }
}
