<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CartCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $total = round($this->collection->reduce(fn($carry, $item) => $carry + $item->product->price, 0), 2);
        $order = $this->collection->first()?->order;
        $cartId = $order ? $order->id : null;

        $result = [
            'id' => $cartId,
            'items' => $this->collection,
            'total' => $total,
        ];

        if (!empty($order->manager)) {
            $result['order'] = $order;
        }

        return $result;
    }
}
