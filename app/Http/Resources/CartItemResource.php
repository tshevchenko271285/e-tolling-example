<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $product = $this->product;
        $category = $product->categories->first();

        return [
            'id' => $this->id,
            'term' => $product->term,
            'start' => $this->started_at->format('d.m.Y'),
            'end' => $this->finished_at->format('d.m.Y'),
            'sign' => $this->car_sign,
            'price' => $product->price,
            'categoryTitle' => $category->title,
            'categoryImage' => $category->image,
        ];
    }
}
