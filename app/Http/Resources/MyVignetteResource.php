<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MyVignetteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $product = $this->product;
        $category = $product->categories->first();

        return [
            'id' => $this->id,
            'term' => $product->term,
            'start' => $this->started_at,
            'end' => $this->finished_at,
            'sign' => $this->car_sign,
            'change_status' => $product->change_status,
            'categoryTitle' => $category->title,
            'categoryImage' => $category->image,
        ];
    }
}
