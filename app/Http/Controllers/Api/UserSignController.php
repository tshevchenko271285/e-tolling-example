<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SignAddRequest;
use App\Http\Requests\SignRemoveRequest;
use App\Http\Resources\UserSignResource;
use App\Services\Contracts\IUserSignService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserSignController extends Controller
{
    /**
     * Injects the required services
     */
    public function __construct(
        protected IUserSignService $userSignService,
    ){}

    /**
     * Returns list of User Signs
     *
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        return UserSignResource::collection($this->userSignService->getAll());
    }

    /**
     * Returns list of User Signs
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function pagination(Request $request): AnonymousResourceCollection
    {
        return UserSignResource::collection($this->userSignService->pagination($request->all()));
    }

    /**
     * Savings a new user sign.
     *
     * @param SignAddRequest $request
     * @return JsonResponse
     */
    public function store(SignAddRequest $request): JsonResponse
    {
        $result = (bool) $this->userSignService->store($request->validated());

        return response()->json(null, $result ? 201 : 400);
    }

    /**
     * Delete the user sign.
     *
     * @param SignRemoveRequest $request
     * @return JsonResponse
     */
    public function remove(SignRemoveRequest $request, $id): JsonResponse
    {
        return response()->json(null, $this->userSignService->remove($id) ? 204 : 404);
    }
}
