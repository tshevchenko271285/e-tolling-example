<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Http\Resources\MyVignetteResource;
use App\Services\Contracts\IProfileService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProfileController extends Controller
{
    /**
     * Injects the required services
     */
    public function __construct(
        protected IProfileService $profileService,
    ){}

    /**
     * Updates data of the current user
     *
     * @param ProfileUpdateRequest $request
     * @return JsonResponse
     */
    public function update(ProfileUpdateRequest $request): JsonResponse
    {
        return response()->json($this->profileService->update($request->except(['password_old'])));
    }

    /**
     * Returns the pagination object of vignettes
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function vignettes(Request $request): AnonymousResourceCollection
    {
        return MyVignetteResource::collection($this->profileService->getVignettes($request->all()));
    }
}
