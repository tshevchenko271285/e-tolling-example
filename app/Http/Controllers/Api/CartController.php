<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CartMakeLinkRequest;
use App\Http\Requests\CartRequest;
use App\Http\Requests\OrderContactsRequest;
use App\Http\Resources\CartCollection;
use App\Http\Resources\CartItemResource;
use App\Services\Contracts\ICartService;
use App\Services\GoogleIPayService;
use App\Services\MasterpassIPayService;
use Illuminate\Http\JsonResponse;

class CartController extends Controller
{
    /**
     * This is where dependencies for controller methods are introduced.
     *
     * @param ICartService $cartService
     */
    public function __construct(
        protected ICartService $cartService
    ) {}

    /**
     * Adding an item to the cart
     *
     * @param CartRequest $request
     * @return JsonResponse
     */
    public function add(CartRequest $request): JsonResponse
    {
        $this->cartService->addItems($request->vignettes);

        return response()->json(null, 200);
    }

    /**
     * Returns data of the cart
     *
     * @return CartCollection
     */
    public function getCart(): CartCollection
    {
        $cart = $this->cartService->getCart();
        $cart->load(['items', 'items.product', 'items.product.categories']);
        $items = CartItemResource::collection($cart->items);

        return new CartCollection($items);
    }

    /**
     * Returns the result of checking the items in the cart
     *
     * @return JsonResponse
     */
    public function hasItems(): JsonResponse
    {
        $result = $this->cartService->hasItems();

        return response()->json($result, 200);
    }

    /**
     * Removes an item from the cart
     *
     * @param $id
     * @return JsonResponse
     */
    public function remove($id): JsonResponse
    {
        return response()->json([], $this->cartService->removeItem($id) ? 204 : 404);
    }

    /**
     * Updates contact`s data of the order
     *
     * @param OrderContactsRequest $request
     * @return JsonResponse
     *
     */
    public function updateContacts(OrderContactsRequest $request): JsonResponse
    {
        return response()->json([], $this->cartService->setContacts($request->validated()) ? 200 : 404);
    }

    /**
     * Returns current success order
     *
     * @return JsonResponse
     */
    public function successOrder(): JsonResponse
    {
        $order = $this->cartService->getSuccessOrder();

        return response()->json($order, $order ? 200 : 404);
    }

    /**
     * Creates the new order and generates a link to add the cart to the user
     *
     * @param CartMakeLinkRequest $request
     * @return JsonResponse
     */
    public function createOrder(CartMakeLinkRequest $request): JsonResponse
    {
        $this->cartService->createOrder($request->validated());

        return response()->json();
    }

    /**
     * Pre-calculates payments fee.
     * This action going to save fee in the cache.
     *
     * @param GoogleIPayService $googleIPayService
     * @param MasterpassIPayService $masterpassIPayService
     * @return JsonResponse
     */
    public function calculationFee(
        GoogleIPayService $googleIPayService,
        MasterpassIPayService $masterpassIPayService
    ): JsonResponse
    {
        return response()->json([
            'google_pay' => $googleIPayService->calculateFee(),
            'masterpass' => $masterpassIPayService->calculateFee(),
        ]);
    }
}
