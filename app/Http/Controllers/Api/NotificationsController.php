<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\Contracts\INotificationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationsController extends Controller
{
    /**
     * Injects the required services
     */
    public function __construct(
        protected INotificationService $notificationService,
    )
    {
    }

    /**
     * Returns the user notifications.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function userNotifications(Request $request): JsonResponse
    {
        return response()->json($this->notificationService->getNotifications($request->all()));
    }

    /**
     * Marks notifications as read
     *
     * @param Request $request
     * @param string|null $id
     * @return JsonResponse
     */
    public function markRead(Request $request, ?string $id = null): JsonResponse
    {
        $this->notificationService->markAsRead($id);

        return response()->json();
    }
}
