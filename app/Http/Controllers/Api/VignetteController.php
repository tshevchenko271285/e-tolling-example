<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApiSignRequest;
use App\Http\Requests\CheckVignetteRequest;
use App\Http\Resources\ApiSignResource;
use App\Services\Contracts\IVignetteService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Cache;

class VignetteController extends Controller
{
    /**
     * The key for caching signs
     */
    const API_SIGN_CACHE_KEY_PREFIX = 'api_signs_';

    /**
     * Signs storage time in the cache.
     */
    const API_SIGN_CACHE_TIME = 120;

    /**
     * Injects necessary services
     * @param IVignetteService $vignetteService
     */
    public function __construct(
        protected IVignetteService $vignetteService
    ) {}

    /**
     * Checks the valid vignette for a car
     *
     * @param CheckVignetteRequest $request
     * @return JsonResponse
     */
    public function check(CheckVignetteRequest $request): JsonResponse
    {
        $vignette = $this->vignetteService->check($request->sign, $request->country);

        return response()->json($vignette, $vignette ? 200 : 404);
    }

    /**
     * Returns the latest vignettes
     *
     * @param ApiSignRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(ApiSignRequest $request): AnonymousResourceCollection
    {
        return Cache::remember(
            self::API_SIGN_CACHE_KEY_PREFIX . $request->getRequestUri(),
            self::API_SIGN_CACHE_TIME,
            fn() => ApiSignResource::collection($this->vignetteService->get($request->validated()))
        );
    }
}
