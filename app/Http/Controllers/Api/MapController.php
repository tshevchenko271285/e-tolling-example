<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoadResource;
use App\Services\Contracts\IRoadService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class MapController extends Controller
{
    /**
     * Returns a list of roads
     *
     * @param IRoadService $roadService
     * @return AnonymousResourceCollection
     */
    public function roads(IRoadService $roadService): AnonymousResourceCollection
    {
        return RoadResource::collection($roadService->getAll());
    }
}
