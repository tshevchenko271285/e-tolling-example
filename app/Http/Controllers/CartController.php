<?php

namespace App\Http\Controllers;

use App\Services\Contracts\ICartService;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Displaying the Page of the shopping cart
     *
     * @return View
     */
    public function show(): View
    {
        return view('pages.cart');
    }

    /**
     * Sets cart from url ID
     *
     * @param Request $request
     * @param ICartService $cartService
     * @return RedirectResponse
     * @throws BindingResolutionException
     */
    public function setCart(Request $request, ICartService $cartService): RedirectResponse
    {
        if ($request->has('id') && $request->hasValidSignature()) {
            $cartService->setCart($request);
        }

        return redirect()->route('cart.show');
    }
}
