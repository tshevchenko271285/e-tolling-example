<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Shows the user profile form
     * @return Application|Factory|View
     */
    public function show(): View|Factory|Application
    {
        return view('pages/profile-show');
    }

    /**
     * Shows the list of user vignettes
     * @return Application|Factory|View
     */
    public function vignettes(): View|Factory|Application
    {
        return view('pages/profile-vignettes');
    }

    /**
     * Displays the user signs
     *
     * @return Application|Factory|View
     */
    public function signs(): View|Factory|Application
    {
        return view('pages/profile-signs');
    }

    /**
     * Displays the user notifications
     *
     * @return Application|Factory|View
     */
    public function notifications(): View|Factory|Application
    {
        return view('pages/profile-notifications');
    }
}
