<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SearchHistoryRequest;
use App\Services\Contracts\IHistoryService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class UserHistoryController extends Controller
{
    /**
     * Injects the HistoryService dependency
     *
     * @param IHistoryService $historyService
     */
    public function __construct(protected IHistoryService $historyService)
    {}

    /**
     * Displays a list of stories sorted by visit id.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('admin.user_histories.index', [
            'histories' => $this->historyService->getList(),
        ]);
    }

    /**
     * Displays a list of actions for a specific story.
     *
     * @param string $historyId
     * @return Application|Factory|View
     */
    public function show(string $historyId): View|Factory|Application
    {
        return view('admin.user_histories.show', [
            'histories' => $this->historyService->getHistoryItems($historyId),
        ]);
    }

    /**
     * Displays detailed information about a user action.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function details(int $id): View|Factory|Application
    {
        return view('admin.user_histories.details', [
            'history' => $this->historyService->getDetails($id),
        ]);
    }

    /**
     * Displays user actions that match the search query.
     *
     * @return void
     */
    public function search(SearchHistoryRequest $request)
    {
        return view('admin.user_histories.show', [
            'histories' => $this->historyService->search($request->all()),
        ]);
    }
}
