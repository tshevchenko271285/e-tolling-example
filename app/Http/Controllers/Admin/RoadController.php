<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoadRequest;
use App\Models\Road;
use App\Services\Contracts\IRoadService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class RoadController extends Controller
{
    /**
     * Dependency Injection required services
     *
     * @param IRoadService $roadService
     */
    public function __construct(
        protected IRoadService $roadService
    )
    {}

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('admin.road.index', [
            'roads' => $this->roadService->paginate(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('admin.road.create', [
            'languages' => config('app.available_locales', [config('app.fallback_locale')]),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoadRequest $request
     * @return RedirectResponse
     */
    public function store(RoadRequest $request): RedirectResponse
    {
        try {
            $this->roadService->create($request->validated());
        } catch (\Exception $e) {
            return redirect()->route('admin.road.create')
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.road.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Road $road
     * @return Application|Factory|View
     */
    public function edit(Road $road): View|Factory|Application
    {
        return view('admin.road.edit', [
            'road' => $road,
            'languages' => config('app.available_locales', [config('app.fallback_locale')]),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoadRequest $request
     * @param Road $road
     * @return RedirectResponse
     */
    public function update(RoadRequest $request, Road $road): RedirectResponse
    {
        try {
            $this->roadService->update($road, $request->validated());
        } catch (\Exception $e) {
            return redirect()->route('admin.road.edit', $road)
                ->with('error', $e->getMessage());
        }

        return redirect()->route('admin.road.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Road $road
     * @return RedirectResponse
     */
    public function destroy(Road $road): RedirectResponse
    {
        try {
            $road->delete();
        } catch (\Exception $e) {
            return redirect()->route('admin.road.index', $e->getMessage());
        }

        return redirect()->route('admin.road.index');
    }
}
