<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\VignetteSearchRequest;
use App\Services\Contracts\IProductService;
use App\Services\Contracts\IVignetteService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class VignetteController extends Controller
{
    /**
     * Injects necessary services
     *
     * @param IVignetteService $vignetteService
     * @param IProductService $productService
     */
    public function __construct(
        protected IVignetteService $vignetteService,
        protected IProductService $productService,
    ) {}

    /**
     * Shows list of the sold vignettes
     *
     * @param VignetteSearchRequest $request
     * @return View
     */
    public function index(VignetteSearchRequest $request): View
    {
        $vignettes = $this->vignetteService->search($request->validated());
        $products = $this->productService->getAll();

        return view('admin.vignette.index', compact('vignettes', 'products'));
    }

    /**
     * Returns the form for creating order.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.vignette.create');
    }
}
