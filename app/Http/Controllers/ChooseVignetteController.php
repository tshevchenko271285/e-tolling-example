<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class ChooseVignetteController extends Controller
{
    /**
     * Returns view with list of the vignettes
     *
     * @return View
     */
    public function index(): View
    {
        return view('pages/vignettes');
    }

    /**
     * Validate list of the vignettes
     *
     * @param CartRequest $request
     * @return JsonResponse
     */
    public function vignettesValidate(CartRequest $request): JsonResponse
    {
        return response()->json(null, 202);
    }
}
