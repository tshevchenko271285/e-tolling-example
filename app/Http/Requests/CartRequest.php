<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class CartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $startToday = Carbon::yesterday()->endOfDay()->format('d.m.Y H:i');

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'PUT':
            case 'PATCH':
            case 'POST':
                return [
                    'vignettes' => ['array', 'required'],
                    'vignettes.*.country' => ['required', 'string', 'max:255', 'min:2'],
                    'vignettes.*.sign' => ['required', 'string', 'max:12', 'min:2'],
                    'vignettes.*.vignetteId' => ['required', 'exists:App\Models\Product,id'],
                    'vignettes.*.start' => ['required', 'date', 'after:' . $startToday],
                ];
            default:
                break;
        }
    }
}
