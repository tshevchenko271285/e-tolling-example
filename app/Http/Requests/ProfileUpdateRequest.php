<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'email' => [
                'required',
                'email:strict',
                'max:255',
                Rule::unique('users')->ignore(\Auth::id()),
            ],
            'surname' => ['nullable', 'string', 'max:255', 'min:2'],
            'phone' => ['required', new PhoneNumber],
            'country' => ['required', 'string', 'max:255', 'min:2'],
            'city' => ['nullable', 'string', 'max:255', 'min:2'],
            'postcode' => ['nullable', 'string', 'max:255', 'min:2'],
            'address' => ['nullable', 'string', 'max:255', 'min:2'],
            'password_old' => ['required_with:password', 'current_password'],
            'password' => [
                'required_with:password_old',
                'string',
                Password::min(8)->numbers()->letters(),
                'confirmed'
            ],
        ];
    }
}
