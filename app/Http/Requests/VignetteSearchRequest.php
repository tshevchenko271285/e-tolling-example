<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VignetteSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Permission::hasAnyPermission([
            'order.index',
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'manager' => ['nullable', 'boolean'],
            'search' => ['nullable', 'string'],
            'date' => ['nullable', 'date'],
            'term' => ['nullable', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'sort' => ['nullable', Rule::in(['id', 'started_at', 'finished_at'])],
            'order' => ['nullable', Rule::in(['DESC', 'ASC'])],
        ];
    }
}
