<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Permission::hasAnyPermission([
            'reminder.index',
            'reminder.update',
            'reminder.create',
            'reminder.delete',
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [

                ];
            case 'PUT':
            case 'PATCH':
            case 'POST':
                return [
                    'title' => ['required', 'string', 'max:255', 'min:1'],
                    'value' => ['required', 'numeric', 'max:65535', 'min:0'],
                ];
            default:
                break;
        }
    }
}
