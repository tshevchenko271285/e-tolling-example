<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class RoadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Permission::hasAnyPermission([
            'road.index',
            'road.update',
            'road.create',
            'road.delete',
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $languages = config('app.available_locales', [config('app.fallback_locale', 'en')]);
        $languagesArrayRule = 'array:'.implode(',', $languages);

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'title' => ['required', $languagesArrayRule],
                    'title.*' => ['required', 'string', 'max:255', 'min:5'],
                    'number' => ['nullable', 'string', 'max:5', 'min:2'],
                    'distance' => ['nullable', 'numeric', 'min:0'],
                    'coordinates' => ['required', File::types(['xml'])->max(8 * 1024)],
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'title' => ['required', $languagesArrayRule],
                    'title.*' => ['required', 'string', 'max:255', 'min:5'],
                    'number' => ['nullable', 'string', 'max:5', 'min:2'],
                    'distance' => ['nullable', 'numeric', 'min:0'],
                    'coordinates' => ['nullable', File::types(['xml'])->max(8 * 1024)],
                ];
            default:
                break;
        }
    }
}
