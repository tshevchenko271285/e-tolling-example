<?php

namespace App\Http\Requests;

use App\Rules\UserSignUnique;
use Illuminate\Foundation\Http\FormRequest;

class SignAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'sign' => ['required', 'string', 'min:2', 'max:12', new UserSignUnique],
        ];
    }
}
