<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class CartMakeLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Permission::hasAnyPermission([
            'order.create',
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $startToday = Carbon::yesterday()->endOfDay()->format('d.m.Y H:i');

        return [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'country' => ['required', 'string', 'max:255', 'min:2'],
            'phone' => ['required', new PhoneNumber],
            'email' => ['required', 'email:strict', 'max:255', 'min:2'],
            'vignettes' => ['array', 'required'],
            'vignettes.*.country' => ['required', 'string', 'max:255', 'min:2'],
            'vignettes.*.sign' => ['required', 'string', 'max:12', 'min:2'],
            'vignettes.*.vignetteId' => ['required', 'exists:App\Models\Product,id'],
            'vignettes.*.start' => ['required', 'date', 'after:'.$startToday],
        ];
    }
}
