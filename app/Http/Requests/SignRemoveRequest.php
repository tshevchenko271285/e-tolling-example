<?php

namespace App\Http\Requests;

use App\Models\User;
use App\Models\UserSign;
use Illuminate\Foundation\Http\FormRequest;

class SignRemoveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $sign = UserSign::with(['user'])->find($this->id);

        return $sign->user?->id === \Auth::id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
