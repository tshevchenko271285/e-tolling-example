<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class CheckLanguage
{
    /**
     * Handle an incoming request.
     * The current locale is set from the URL. If the URL does not contain a locale,
     * it will be redirected to the previously selected locale or the default locale.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->lang, config('app.available_locales', []))) {
            \App::setLocale($request->lang);
            session(['lang' => $request->lang]);

            URL::defaults(['lang' => $request->lang]);
        } else {
            $langPrefix = session('lang', config('app.fallback_locale', 'en'));

            $redirectUrl = str_replace('//', '/', $langPrefix . '/' . $request->path());
            if ($request->getQueryString()) {
                $redirectUrl .= '?' . $request->getQueryString();
            }

            Log::debug('Language redirect from "' . $request->path() . '" to "' . $redirectUrl . '". The selected language is - ' . $langPrefix);

            return redirect($redirectUrl, 302, $request->headers->all());
        }

        return $next($request);
    }
}
