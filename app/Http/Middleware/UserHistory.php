<?php

namespace App\Http\Middleware;

use App\Services\Contracts\IHistoryService;
use Closure;
use Illuminate\Http\Request;

class UserHistory
{
    /**
     * Inject History Service
     * @param IHistoryService $historyService
     */
    public function __construct(
        protected IHistoryService $historyService
    )
    {}

    /**
     * Saves data about each request to the user's history
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $this->historyService->add();

        return $next($request);
    }
}
