<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;

class CheckApiLanguage
{
    /**
     * Handle an incoming request.
     * The current locale is set from the URL. If the URL does not contain a locale,
     * it will be redirected to the previously selected locale or the default locale.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = $request->header('app-locale', config('app.fallback_locale'));

        \App::setLocale($lang);

        URL::defaults(['lang' => $lang]);

        return $next($request);
    }
}
