<?php

namespace App\Rules;

use App\Models\UserSign;
use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Support\Facades\Auth;

class UserSignUnique implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        $sign = UserSign::where('sign', $value)->first();
        $userId = Auth::id();

        if ($sign && $userId) {
            if ($sign->user_id === $userId) {
                $fail('validation.custom.user_sign_unique.you_have')->translate();
            } else {
                $fail('validation.custom.user_sign_unique.another_has')->translate();
            }
        }
    }
}
