<?php

namespace App\Observers;

use App\Models\Order;
use App\Notifications\CartCreatedNotification;
use Illuminate\Support\Facades\Notification;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param Order $order
     * @return void
     */
    public function created(Order $order)
    {
        if (!empty($order->manager)) {
            $name = $order->name . ' ' . $order->surname;
            $email = $order->email;
            Notification::route('mail', [$email => $name])->notify(new CartCreatedNotification($order));
        }
    }
}
