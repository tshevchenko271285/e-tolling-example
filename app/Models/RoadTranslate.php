<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RoadTranslate extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['road_id', 'lang', 'title'];

    /**
     * Get the Road that owns the translation.
     *
     * @return BelongsTo
     */
    public function road(): BelongsTo
    {
        return $this->belongsTo(Road::class);
    }
}
