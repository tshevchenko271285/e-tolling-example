<?php
namespace App\Models\Traits;

use Illuminate\Support\Facades\Crypt;

trait CryptAttributes
{
    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if (in_array($key, ($this->encryption ?? []))) {
            try {
                $value = Crypt::decryptString($value);
            } catch (\Throwable $e) {
                report($e);
            }
        }

        return $value;
    }

    public function setAttribute($key, $value)
    {
        if (in_array($key, ($this->encryption ?? []))) {
            try {
                $value = Crypt::encryptString($value);
            } catch (\Throwable $e) {
                report($e);
            }
        }

        return parent::setAttribute($key, $value);
    }
}
