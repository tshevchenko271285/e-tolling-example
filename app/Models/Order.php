<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Itmaster\Manager\Models\Traits\AccessTrait;

class Order extends Model
{
    use HasFactory, AccessTrait;

    const TYPE_OF_PERSON_PRIVATE = 'private';
    const TYPE_OF_PERSON_BUSINESS = 'business';

    const PAYMENT_TYPE_ONETIME = 'one-time';
    const PAYMENT_TYPE_REGULAR = 'regular';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type_of_person',
        'name',
        'surname',
        'country',
        'postcode',
        'address',
        'city',
        'phone',
        'email',
        'company_name',
        'tax_number',
        'payment_type',
        'paid',
    ];

    /**
     * The model's default values for attributes.
     *
     * @var array
     */
    protected $attributes = [
        'type_of_person' => self::TYPE_OF_PERSON_PRIVATE,
        'payment_type' => self::PAYMENT_TYPE_ONETIME,
    ];

    /**
     * Get the items of this order.
     */
    public function items(): HasMany
    {
        return $this->hasMany(OrderItem::class)->orderBy('created_at', 'desc');
    }

    /**
     * Get the user that owns the order.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the manager that owns the order.
     */
    public function manager(): BelongsTo
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    /**
     * Returns the total order amount
     *
     * @return Attribute
     */
    protected function amount(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->items()->sum('product_data->price'),
        );
    }
}
