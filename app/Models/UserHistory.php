<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * User History Model
 */
class UserHistory extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'visit_id',
        'page',
        'referer',
        'parent_id',
        'ip',
        'auth_type',
        'user_id',
        'get',
        'post',
        'message',
        'event_type',
    ];

    /**
     * Value for attribute method.
     *
     * @var string
     */
    const METHOD_POST = 'POST';

    /**
     * Value for attribute method.
     *
     * @var string
     */
    const METHOD_GET = 'GET';

    /**
     * Current User History
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the last activity of the current visit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lastVisit(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(self::class, 'visit_id', 'visit_id')->orderBy('created_at', 'DESC');
    }

    /**
     * Get the request method
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function method(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => !empty($this->post) ? self::METHOD_POST : self::METHOD_GET,
        );
    }
}
