<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Road extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['number', 'title', 'distance', 'coordinates'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'coordinates' => 'object'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['translate'];

    /**
     * Returns translate of the road.
     *
     * @return HasMany
     */
    public function translate(): HasMany
    {
        return $this->hasMany(RoadTranslate::class)->where('lang', \App::getLocale());
    }

    /**
     * Get all translations for the Road.
     *
     * @return HasMany
     */
    public function translates(): HasMany
    {
        return $this->hasMany(RoadTranslate::class);
    }

    /**
     * Returns the translation for the title
     *
     * @return Attribute
     */
    protected function title(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => $this->translate->first()?->title ?? '',
        );
    }

    /**
     * Returns the desired translation
     *
     * @param $query
     * @param $language
     * @return Model
     */
    public function scopeGetTranslate($query, $language): Model
    {
        return $this->translates()->where('lang', $language)->first();
    }
}
